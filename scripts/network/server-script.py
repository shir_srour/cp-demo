import time                                                        
import socket                                                      
import threading                                                   
                                                                    
def open_tcp_socket_for_listening(host, number_of_packets, port=0): 
    """                                                             
    opening listening in socket                                     
    :param host: host ip                                            
    :type host: str                                                 
    :param port: port number (0 by default = random not in use port)
    :type port: int                                        
    :param number_of_packets: max packets to listen                     
    :type number_of_packets: int                                        
    :return: socket and process objects                                 
    """                                                                 
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)             
    soc.bind((host, port))
    proc = threading.Thread(target=soc.listen, args=[number_of_packets])
    proc.start()                                                   
    listen_port = soc.getsockname()[1] if port == 0 else port           
    return soc, proc, listen_port                                   
                                                                        
if __name__=='__main__':  
    soc_proc_port = {}                                                  
    soc, proc, port = open_tcp_socket_for_listening(host='0.0.0.0', number_of_packets=128,
                                                    port=45200)         
    soc_proc_port[0] = (soc, proc, 45200, True, 0)                                        
    for i in range(1, 2):                                              
        soc, proc, port = open_tcp_socket_for_listening(host='0.0.0.0', number_of_packets=128,
                                                        port=45200 + i) 
        soc_proc_port[i] = (soc, proc, port, False, 128)                                      
                                                                        
    time.sleep(300)


