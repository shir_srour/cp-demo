#!/usr/bin/env python
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import argparse
import collections
import sys
import time
import traceback

import yaml
from yaml.loader import SafeLoader

HOURS_IN_A_DAY = 24
MINUTES_IN_A_DAY = HOURS_IN_A_DAY * 60
SECONDS_IN_A_DAY = MINUTES_IN_A_DAY * 60


class SafeLineLoader(SafeLoader):
    """
    A special loader for the yaml protocol,
    that remembers the line numbers where mappings are placed.
    This allows to provide the user with a hint to the error location.
    Errors in the yaml file may be logical and not only syntactic.
    """
    line_numbers = dict()

    def construct_mapping(self, node, deep=True):
        mapping = super(SafeLineLoader, self).construct_mapping(
            node, deep=deep)
        d = dict()
        for key_node, value_node in node.value:
            d[key_node.value] = value_node.start_mark.line + 1
        SafeLineLoader.line_numbers[str(sorted(mapping.items()))] = d
        return mapping

    @staticmethod
    def get_line_number(mapping, key=None):
        mapping_line_numbers = SafeLineLoader.line_numbers[str(
            sorted(mapping.items()))]
        if key not in mapping_line_numbers:
            return min(mapping_line_numbers.values())
        return mapping_line_numbers[key]


def print_freq(packets_per_time_frame, time_frame_in_seconds):
    """
    Function returns a string representation of the frequency,
    divided by the smallest time unit possible (between second, minute, hour or day)

    Frequencies higher than 0, and lower than 1 per day (1 packet per 86400 seconds)
    cause an exception to be thrown.
    """
    hertz = float(packets_per_time_frame) / time_frame_in_seconds

    per_day = int(hertz * SECONDS_IN_A_DAY)

    if 0 == per_day and (0 != packets_per_time_frame):
        raise AssertionError("Frequency requested is not supported.")

    if 0 == per_day % SECONDS_IN_A_DAY:
        return str(int(per_day / SECONDS_IN_A_DAY)) + "/sec"

    if 0 == (per_day % MINUTES_IN_A_DAY):
        return str(int(per_day / MINUTES_IN_A_DAY)) + "/min"

    if 0 == (per_day % HOURS_IN_A_DAY):
        return str(int(per_day / HOURS_IN_A_DAY)) + "/hour"

    return str(per_day) + "/day"


class Rule(object):
    def __init__(self):
        self.chain = ''
        self.target = ''
        self.match = ''
        self.direction = None
        self.protocol = ''

    def __str__(self):
        if not self.chain:
            raise AssertionError("Cannot print rule that has no chain")
        return ' '.join(' '.join([self.chain, self.match, self.target]).split())

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        # compare by value
        return self.__dict__ == other.__dict__

    def set_chain(self, value):
        assert isinstance(value, str), "chains are specified as strings"
        assert (len(
            value)) < 29, "Length of chain names must be shorter or equal than 29 characters"
        self.chain = value
        return self

    def jump_to(self, value):
        if self.target:
            raise AssertionError("Cannot specify two targets for one rule")
        self.target = '-j ' + value
        return self

    def target_log_self_chain(self):
        if self.target:
            raise AssertionError("Target already specified")
        if not self.chain:
            raise AssertionError(
                "Rule cannot log it's chain name because the chain name has not been specified yet")
        self.target = '-j LOG --log-prefix "{} "'.format(self.chain)
        return self

    def set_direction(self, value):
        if value not in ['INPUT', 'OUTPUT']:
            raise ConfigError(
                'direction', '"{}" is not a supported direction'.format(value))
        self.direction = value

        return self

    def rate_limit(self, limit, time_frame):
        if not isinstance(limit, int):
            raise ConfigError('limit', "limit must be an integer")
        if limit < 0:
            raise ConfigError('limit', "limit cannot be negative")
        if time_frame <= 0:
            raise ConfigError('time_frame_in_seconds',
                              "time frame must be positive")
        if not isinstance(time_frame, int):
            raise ConfigError('time_frame_in_seconds',
                              "time frame must be an integer")
        if not time_frame <= 86400:
            raise ConfigError(
                'time_frame_in_seconds', "time frame must be lower than 86400 seconds (1 day)")
        frequency = print_freq(limit, time_frame)
        self.match += ' -m limit --limit {} --limit-burst {}'.format(
            frequency, limit)
        return self

    def add_match(self, value):
        self.match += " " + value
        return self

    def set_interface(self, value):
        if not self.direction:
            raise AssertionError(
                "Cannot set rule interface name before rule direction")
        if self.direction == 'INPUT':
            self.match += ' -i ' + value
        else:
            self.match += ' -o ' + value
        return self

    def set_protocol(self, value):
        if value not in ['TCP', 'UDP']:
            raise ConfigError('protocol', str(value) +
                              " is not a supported protocol")
        self.protocol = value.lower()
        self.match += ' -p ' + self.protocol + ' -m ' + self.protocol
        return self

    def set_source_port(self, value):
        if not isinstance(value, int):
            raise ConfigError(
                'source_port', "source port must be specified as an integer or \"all\"")
        if not (value < 2 ** 16 - 1):
            raise ConfigError(
                'source_port', "source port must be lower than " + str(2 ** 16 - 1))
        if value == 0:
            raise ConfigError(
                'source_port', '0 is not supported as a port number. for all ports, use "all"')
        if value < 0:
            raise ConfigError(
                'source_port', 'source port cannot be negative')
        if not self.protocol:
            raise ConfigError(
                'source_port', "Cannot set source port filter before protocol filter")
        self.match += ' --sport '
        self.match += str(value)
        return self

    def set_destination_port(self, value):
        if not isinstance(value, int):
            raise ConfigError(
                'destination_port', "destination port must be specified as an integer or \"all\"")
        if not (value < 2 ** 16 - 1):
            raise ConfigError(
                'destination_port', "destination port must be lower than " + str(2 ** 16 - 1))
        if value == 0:
            raise ConfigError(
                'destination_port', '0 is not supported as a port number. for all ports, use "all"')
        if value < 0:
            raise ConfigError(
                'destination_port', 'destination port cannot be negative')
        if not self.protocol:
            raise ConfigError(
                'destination_port', "Cannot set destination port filter before protocol filter")
        self.match += ' --dport '
        self.match += str(value)
        return self


class ConfigError(AssertionError):
    def __init__(self, key, msg):
        super(ConfigError, self).__init__(msg)
        self.key = key

    def __repr__(self):
        return super(ConfigError, self).__repr__()


def is_chain_built_in(chain_name):
    return chain_name in ['PREROUTING', 'INPUT', 'FORWARD', 'POSTROUTING', 'OUTPUT']


def is_user_defined_chain(chain_name):
    return not is_chain_built_in(chain_name)


def extract_rules_for_synflood_detection(feature_config):
    rules = []
    for interface_name in feature_config['MonitoredNetworkInterfaces']:
        interface_config = feature_config['MonitoredNetworkInterfaces'][interface_name]
        try:
            rules.append(
                Rule().set_chain('ARGUS_SYN_FLOOD').set_direction('INPUT').set_interface(interface_name).rate_limit(
                    interface_config['limit'], interface_config['time_frame_in_seconds']).jump_to('RETURN'))
            rules.append(Rule().set_chain('ARGUS_SYN_FLOOD').set_direction('INPUT').set_interface(
                interface_name).rate_limit(1, interface_config['time_frame_in_seconds']).target_log_self_chain())
        except ConfigError as config_error:
            info = "(See line number {})".format(
                SafeLineLoader.get_line_number(interface_config, config_error.key))
            if config_error.args:
                info = str(config_error.args[0]) + ' ' + info
            config_error.args = tuple([info])

            raise
    if rules:
        rules.insert(0, Rule().set_chain('INPUT').set_protocol('TCP').add_match(
            '--tcp-flags FIN,SYN,RST,ACK SYN').jump_to('ARGUS_SYN_FLOOD'))
    return rules


def extract_rules_for_packet_rate(feature_config):
    rules = []

    rate_ids = dict()
    for rate in feature_config:
        try:
            rate_id = rate['interface_name'], rate['direction'], rate['protocol'], rate['source_port'], rate['destination_port']
            if rate_id in rate_ids:
                raise ConfigError(None,"Packet Rate parameters defined in line {} were illegally redefined again".format(rate_ids[rate_id]))
                
            rate_ids[rate_id] = SafeLineLoader.get_line_number(rate)
            rule1 = Rule().set_direction(
                rate['direction']).set_interface(rate['interface_name'])
            rule2 = Rule().set_direction(
                rate['direction']).set_interface(rate['interface_name'])

            chain = 'ARGUS_PACKET_RATE'

            specific_protocol = None
            rule_is_port_specific = False
            if rate['protocol'] and rate['protocol'] != 'all':
                specific_protocol = rate['protocol']
                rule2.set_protocol(specific_protocol)
                rule1.set_protocol(specific_protocol)

            if rate['source_port'] and rate['source_port'] != 'all':
                chain += '_SPT'
                rule_is_port_specific = True
                rule1.set_source_port(rate['source_port'])
                rule2.set_source_port(rate['source_port'])

            if rate['destination_port'] and rate['destination_port'] != 'all':
                if rule_is_port_specific:
                    raise ConfigError('destination_port',
                                      'Cannot specify both source_port={} and destination_port={}'.format(
                                          rate['source_port'], rate['destination_port']))
                chain += '_DPT'
                rule_is_port_specific = True
                rule1.set_destination_port(rate['destination_port'])
                rule2.set_destination_port(rate['destination_port'])

            if not specific_protocol and rule_is_port_specific:
                raise ConfigError(
                    'protocol', 'Cannot specify port value without specifying a protocol value.')
            elif specific_protocol and not rule_is_port_specific:
                raise ConfigError(
                    'protocol',
                    'Cannot specify protocol={} without specifying any port numbers'.format(specific_protocol))

            from_hook = Rule().set_chain(rate['direction']).jump_to(chain)
            if from_hook not in rules:
                rules = [from_hook] + rules

            rule1.set_chain(chain) \
                .jump_to('RETURN')

            rule2.set_chain(chain) \
                .target_log_self_chain()

            # Order of matches is important: the limit module must be last,
            # or else the limit will be wasted on packets that did not match the rest of the rule anyway.
            rule1.rate_limit(rate['limit'], rate['time_frame_in_seconds'])
            rule2.rate_limit(1, rate['time_frame_in_seconds'])

            rules.append(rule1)
            rules.append(rule2)
        except ConfigError as config_error:
            info = "(at line number {})".format(
                SafeLineLoader.get_line_number(rate, config_error.key))
            if config_error.args:
                info = str(config_error.args[0]) + ' ' + info
            config_error.args = tuple([info])

            raise
    return rules


def extract_rules(path_to_yaml):
    with open(path_to_yaml, 'r') as stream:
        configuration = yaml.load(stream, Loader=SafeLineLoader)

    rules = []

    rules += extract_rules_for_synflood_detection(configuration['SynFlood'])
    rules += extract_rules_for_packet_rate(
        configuration['HighPacketRate']['MonitoredPacketRates'])

    for r in rules:
        if r.chain == 'ARGUS_IN':
            rules.insert(0, Rule().set_chain('INPUT').jump_to('ARGUS_IN'))
            break
    for r in rules:
        if r.chain == 'ARGUS_OUT':
            rules.insert(0, Rule().set_chain('OUTPUT').jump_to('ARGUS_OUT'))
            break
    return rules


def into_bash_commands(rules, table, action, executables):
    commands = []

       
    for executable in executables:
        prefix = executable + ' -t ' + table + ' '
        user_chains = set()
        for r in rules:
            if is_user_defined_chain(r.chain):
                user_chains.add(r.chain)

        if action == 'start':
            for chain in user_chains:
                commands += [prefix + '-N ' + chain]
            for rule in rules:
                if is_chain_built_in(rule.chain):
                    # Existing chains may have existing rules.
                    # We want our rules to run before them
                    commands += [prefix + '-I ' + str(rule)]
                else:
                    commands += [prefix + '-A ' + str(rule)]
        else:
            for rule in rules:
                commands += [prefix + '-D ' + str(rule)]
            for chain in user_chains:
                commands += [prefix + '-X ' + chain]
    return commands


def into_iptables_restore(rules, table, executables):
    """
    Formats a list of rules as the contents of a text file,
    in the iptables-restore format.
    User defined chain names are deduced from rules and created.
    """
    lines = ['# Generated by Argus on ' + time.ctime(), '*' + table]
    chains = collections.OrderedDict()

    for r in rules:
        if is_chain_built_in(r.chain):
            # Existing chains may have existing rules.
            # We want our rules to run before them
            chains.setdefault(r.chain, []).append('-I ' + str(r))
        else:
            chains.setdefault(r.chain, []).append('-A ' + str(r))

    for chain in chains:
        if is_user_defined_chain(chain):
            lines.append(':' + chain + ' - [0:0]')

    for chain in chains:
        for rule in chains[chain]:
            lines.append(rule)

    lines.append('COMMIT')
    lines.append('# Completed on ' + time.ctime())

    restore_format = '\n'.join(lines)
    output = ""
    for executable in executables:
        output += 'echo "{}" | {} --noflush\n\n'.format(restore_format.replace('"','\\\"'), executable) 
    return output

def get_executables(path_to_yaml, use_restore):
    # default scenario is a non absolute path, will work most of the time
    path_to_binary = "xtables-multi"
    ipv4_support = False
    ipv6_support = False
    with open(path_to_yaml, 'r') as stream:
            configuration = yaml.load(stream, Loader=SafeLineLoader)
            path_to_binary = configuration["IPTablesMonitor"]['path']
            ipv4_support = bool(configuration["IPTablesMonitor"]['ipv4'])
            ipv6_support = bool(configuration["IPTablesMonitor"]['ipv6'])
    executables = []
    if ipv4_support:
            executables += [path_to_binary + " iptables"]
    if ipv6_support:
            executables += [path_to_binary + " ip6tables"]
    
    if use_restore:
        executables = [e+"-restore" for e in executables]
    return executables
    
def main():
    """
    Parses arguments and calls xor_file.
    :return:
    """
    parser = argparse.ArgumentParser(
        description="Add IPtables rules from yaml")
    parser.add_argument("-table", help="Table to add rules into. Default is filter ",
                        choices=['filter', 'nat', 'mangle', 'security', 'raw'])
    parser.add_argument("action", choices=['start', 'stop'])
    parser.add_argument("yaml", help="ATD's yaml configuration file", type=str)
    parser.add_argument(
        "--restore",
        help="output iptables-restore format instead of bash commands. Available only with 'start' selected",
        action='store_true')
    parser.add_argument(
        "--systemd",
        help="output iptables format as bash commands with relevant service prefix.",
        action='store_true')
    parser.set_defaults(table='filter', action=True)
    args = parser.parse_args()
    
    if args.restore and args.action == 'stop':
        raise AssertionError(
            "Error: iptables-restore is only used to load rules, not unload rules. The --restore option is not "
            "available with 'stop'")
    
    executables = get_executables(args.yaml, args.restore)
        
    rules = extract_rules(args.yaml)
    if args.restore:
        print into_iptables_restore(rules, args.table, executables)
        return
    elif args.systemd:
        if args.action == 'stop':
            prefix = "ExecStop=/bin/sh -c"
            concat = " ; "
        else:
            prefix = "ExecStart=/bin/sh -c"
            concat = " && "
        iptables_rules = into_bash_commands(rules, args.table, args.action, executables)
        if len(iptables_rules) > 0:
            print prefix + ' "' + concat.join(iptables_rules).replace(r'"', r'\"') + '"'
    else:
        print '"' + concat.join(into_bash_commands(rules, args.table, args.action, executables)).replace(r'"', r'\"') + '"'

if __name__ == "__main__":
    try:
        main()
        sys.exit(0)
    except Exception as e:
        traceback.print_exc(e)
        sys.exit(-1)
