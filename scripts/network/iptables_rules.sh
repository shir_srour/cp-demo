#!/bin/bash
####################################
#  Creates "argus_iptables.service" with the relevant initial values
####################################
yaml=$1                             # ATD configuration file
output=$2                           # Where to write service
template=$3                         # Service template file
create_iptables_rules_script=$4     # Path to create_iptables_rules.py
start_rules="$(fakeroot python $create_iptables_rules_script start $yaml --systemd)"
stop_rules="$(fakeroot python $create_iptables_rules_script stop $yaml --systemd)"  
type="oneshot"
remain_after_exit="yes"
wanted_by="multi-user.target"
bind_to="argus.service"
refuseManualStop="no"
description="ARGUS Iptables pre start and post stop Service"
template_content="$(cat $template)"
eval "echo \"${template_content}\"" > $output