#!/usr/bin/env python3

from os import listdir, curdir
from os.path import isfile, join, abspath
import subprocess

user = listdir("/media/")[0]
path = join("/media", user)
sub = [d for d in listdir(path) if d != "raspberrypi"][0]
subdir = join(path, sub)
print(subdir)
curpath = abspath(curdir)
with open(join(curpath, "specific_files_list.txt"), "w+") as f:
	with open(join(curpath, "basic_specific_files_list.txt"), "r") as basicfile:
		line = basicfile.readline()
		while line:
			f.write(subdir + line)
			line = basicfile.readline()

print("signing: ", subdir)
subprocess.Popen("python3 ./crypto/sign_fs.py -p {file} -f -s ./specific_files_list.txt".format(file=subdir).split())






