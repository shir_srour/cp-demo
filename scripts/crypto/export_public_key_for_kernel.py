#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

# pip install cryptography pycrypto

from __future__ import print_function
from cryptography.hazmat.primitives.serialization import PublicFormat
from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.hazmat.primitives.serialization import load_der_private_key
from cryptography.hazmat.backends import default_backend
from cryptography import x509
from Crypto.Util.number import long_to_bytes
import argparse
import os
import base64


PRIVATE_KEY = "private"
CERTIFICATE = "cert"
FAILURE = 1


def extract_public_key(path, key_type):
    '''Extract public key from the given source, either private key or certificate

    :param path: path to data source
    :param key_type: data source type (private key / certificate)
    :raises Exception: In case of fault data source type (neither private key, nor certificate)
    :return: public key
    '''

    if key_type == PRIVATE_KEY:
        private_key_str = open(path).read()
        # remove BEGIN and END lines
        b64data = '\n'.join(private_key_str.splitlines()[1:-1])
        derdata = base64.b64decode(b64data)
        key = load_der_private_key(derdata, None, default_backend())
        public_key = key.public_key()
    elif key_type == CERTIFICATE:
        cert = x509.load_pem_x509_certificate(open(path).read(), default_backend())
        public_key = cert.public_key()
    else:
        raise Exception("Invalid key type provided, the type should be either '{}' or '{}'.".format(PRIVATE_KEY, CERTIFICATE))
    return public_key


def num_to_hexcode(number):
    '''Convert a given number to hexadecimal, and format it in chunks

    :param number: the number to convert
    :return: converted hexadecimal code
    '''

    # convert to hex without '0x' prefix or 'L' suffix
    num_hex = format(number, 'x')
    # pad even digits with zeroes
    num_hex_padded = num_hex.zfill(len(num_hex) + len(num_hex) % 2)
    # split into 2-digit chunks and prepend '0x' prefixes
    num_hex_formatted = "0x" + ", 0x".join(num_hex_padded[i: i + 2] for i in range(0, len(num_hex_padded), 2))
    return num_hex_formatted


def num_to_base64code(number):
    '''Convert a given number to base64 representation

    :param number: the number to convert
    :return: converted base64 code
    '''

    num_byte_code = long_to_bytes(number)
    num_base64_code = base64.b64encode(num_byte_code)
    return num_base64_code


def main():
    parser = argparse.ArgumentParser(description="Extract public key, modulus and exponent data formatted for usage in ASE/LSM, from a private key or from a certificate.")
    parser.add_argument("key_path", help="Path to a Private RSA Key or a Certificate to extract the data from", type=str)
    parser.add_argument("-t", "--type", help="key type ('{}' or '{}')".format(PRIVATE_KEY, CERTIFICATE), type=str, required=True)
    args = parser.parse_args()

    try:
        public_key = extract_public_key(args.key_path, args.type)
    except Exception as e:
        print("Extraction aborted. Error:\n{}".format(e))
        exit(FAILURE)

    public_key_bytes = public_key.public_bytes(Encoding.DER, PublicFormat.PKCS1)
    modulus = public_key.public_numbers().n
    exponent = public_key.public_numbers().e

    public_key_hex_py2 = "0x" + ", 0x".join([x.encode('hex') for x in public_key_bytes])
    # public_key_hex_py3 = "0x" + ", 0x".join("%02x" % b for b in public_key_bytes)
    public_key_base64 = base64.b64encode(public_key_bytes)

    print("Public key in hex:\n{}\n".format(public_key_hex_py2))
    print("Public key in base64:\n{}\n".format(public_key_base64))

    print("Modulus in hex:\n{}\n".format(num_to_hexcode(modulus)))
    print("Modulus in base64:\n{}\n".format(num_to_base64code(modulus)))

    print("Exponent in hex:\n{}\n".format(num_to_hexcode(exponent)))
    print("Exponent in base64:\n{}\n".format(num_to_base64code(exponent)))


if __name__ == "__main__":
    main()
