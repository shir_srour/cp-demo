Table of Contents
=================
   * [General Description](#markdown-header-general-description)
   * [Prerequisites](#markdown-header-prerequisites)
   * [Signing](#markdown-header-signing)
      * [Preparations](#markdown-header-preparations)
      * [Sign rootfs](#markdown-header-sign-rootfs)
         * [Sign with defaults](#markdown-header-sign-with-defaults)
         * [Sign with multiple specific files](#markdown-header-sign-with-multiple-specific-files)
      * [Required Arguments](#markdown-header-required-arguments)
         * [Paths](#markdown-header-paths)
      * [Optional Flags](#markdown-header-optional-flags)
      * [Examples of Usage](#markdown-header-examples-of-usage)
         * [Ignore specific_files_list](#markdown-header-ignore-specific_files_list)
         * [Force sign a file](#markdown-header-force-sign-a-file)
   * [Generating new keys for signing and verification](#markdown-header-generating-new-keys-for-signing-and-verification)
   * [Extracting public key for LSM and ASE configuration](#markdown-header-extracting-public-key-for-lsm-and-ase-configuration)


# General Description

The `sign_fs.py` script is designed to cryptographically sign files across the filesystem to be deployed.
The following files are signed in the process: 

 - files with execution permission
 - files with ELF header
 - files with '#!' header
 - files listed in the `specific_files_list` *
 
The signature (encrypted hash of the file) is stored in files extended attributes.

Pay attention to use tools that support xattr (extended attributes) when handling the signed files (packing/unpacking) later on.

\* Signing specific files is an essential part of the proper signing procedure, as ASE will attempt to verify files as configured in `ase_config.yaml` at runtime.

# Prerequisites #

`sudo apt-get install attr`

Install required Python modules, run:

`pip install -r requirements.txt`

# Signing #

## Preparations ##

 - generate `specific_files_list` file based on the ASE configuration file *.
 - have rootfs directory tree ready for signing in accessible location
 - have public `key_file`** ready for signing in accessible location


\* `specific_files_list` should be always generated from the data in   
`argus-lsm/dynamic-config/<SYSTEM_TYPE>/ase_config.yaml` configuration file.  
To generate the list run: `argus-lsm/scripts/config/extract_list_from_yaml_conf.py`.  
During a normal signing procedure, specific_files_list should never be empty.   
If you don't want to sign any additional files, set the config to use an empty file. 

Multiple specific_files_list pathes are supported.  

\*\* `key file`:
The current key file is stored in the dynamic_config/development/hips.key in argus-lsm repository  

 
## Sign rootfs ##

### Sign with defaults ###
To sign the root_fs with default configuration for `key_file` and `specific_files_list`  
run the following command from argus-lsm/scripts/crypto/:

`sudo sign_fs.py -p ~/path/to/sign/directory/`  

The sign_fs is the standard mode for signing, that includes all the sign stages:  

- analyze the file system from given path, complying with the selected arguments  
- generate signatures for all the files that passed the analysis  
- deploy the generated signatures for each file at the selected paths.  

- Default key for signing is set to `hips.key` under dynamic_config/development/hips.key in argus-lsm repository  
- Default specific_files_list for signing is set to `specific_files_list.txt` under dynamic_config/development/specific_files_list.txt  
Make sure to generate the `specific_files_list.txt` as described in [Preparations](#markdown-header-preparations)  

### Sign with multiple specific files ###
To sign the root_fs with as many `specific files paths` as required and the default configuration for `key_file` run the following command from argus-lsm/scripts/crypto/:

`sudo sign_fs.py -p path/to/sign/directory/ -s path/to/specific_file_1 -s path/to/specific_file_2`  

\* any amount of specific files paths is supported, each path should be preceeded by `-s` argument


## Required Arguments ##
All required arguments and flags can be overridden  

### Paths ##
`-p`,  `--path_to_sign`              
`-s`, `--specific_files_list_path`         
`-k`, `--key_path`                     

## Optional Flags ##
`-n`, `--no_specific_files` -  do not require path to the `specific_file_list.txt`, ignore the `specific_file_list.txt` if provided
`-f`, `--force`  -  force sign a directory or force sign a single file                 


## Examples of Usage ##
\* all paths are relative to current path: argus-lsm/scripts/crypto/ 
 
#### Ignore specific_files_list: ####

`sudo sign_fs.py -p ~/path/to/sign/directory/ -n`

- signs all signable files from the specified file along the resulting directory tree
- `-n` flag allows for not specifying the `specific_files_list_path`  


#### Force sign a file: ####

`sudo sign_fs.py -p ~/path/to/sign/directory/file_name -f`

- signs the file at the given path, even if it is not signable by definition


# Generating new keys for signing and verification #

- Generating private key:  
   `openssl genrsa 2048 -noout -keyout > hips.key`  
- Generating a certificate:  
   `openssl req -new -x509 -key existing_private.key -out new_certificate.crt`  

# Extracting public key for LSM and ASE configuration
Extracting public key, modulus and exponent for LSM/ASE code/configuration

 -  To extract data using private key as a source, run:
    
    `./export_public_key_for_kernel.py /path/to/hips.key -t private`
 
 -  To extract data using certificate as a source, run:
    
    `./export_public_key_for_kernel.py /path/to/hips.crt -t cert`

\* Additional info is available here
 http://stackoverflow.com/questions/41084118/crypto-akcipher-set-pub-key-in-kernel-asymmetric-crypto-always-returns-error

