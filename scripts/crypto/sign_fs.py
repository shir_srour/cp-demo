#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import argparse
import os.path
from subprocess import call
from sys import stdin, stdout, stderr


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
ARGUS_LSM_DIR = os.path.dirname(os.path.abspath(
    os.path.join(__file__, "../..")))
DEV_DIR = os.path.realpath(os.path.join(
    ARGUS_LSM_DIR, "keys"))


def configure_args():
    '''
    Parses command line arguments to specify the flow
    :param[in] path_to_sign: rootfs path to sign, or a file
    :param[in] force: flag to indicate that specified path is to pe processed
    :param[in] extended_signature: flag to indicate files to be signed
                                   with advanced options
    :param[in] include_absolute_path_in_signature: flag to indicate file's
               content to be signed with its absolute path,
               relevant only if --extended_signature is set
    :param[in] no_specific_files: flag to indicate that no specific files
                                  are provided
    :param[in] specific_files_list_path: path to specific_files_list
    :param[in] key_path: path to file with key for generate flow
    :type[in]: str
    :return: parsed command line arguments
    '''
    parser = argparse.ArgumentParser(
        description="Import Signatures Flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", "--path_to_sign", required=True, type=str,
                        help="directory tree or a file to sign")
    parser.add_argument("-f", "--force", action='store_true',
                        default=False, help="force sign provided path")
    parser.add_argument("--extended_signature", action='store_true',
                        default=False, help="sign with advanced options")
    parser.add_argument("--include_absolute_path_in_signature",
                        action='store_true', default=False,
                        help="sign file's content and its path, "
                             "relevant only if --extended_signature is set")
    parser.add_argument("-n", "--no_specific_files", action='store_true',
                        default=False,
                        help="do not sign files from specific_file_list")
    parser.add_argument("-s", "--specific_files_list_path",
                        action='append', default=None,
                        help="sign files from specific_file_list,"
                             "multiple files can be provided")
    parser.add_argument("-k", "--key_path", help="key file path ",
                        default=os.path.realpath(os.path.join(
                            DEV_DIR, "hips.key")), type=str)

    return parser.parse_args()


def main():
    '''
    Wrapper to the current signing script. 
    Provides default values for required arguments in development
        ! Runs the sign_fs mode only
    Creates the command line instruction and forwards it to
    the actual signing script in sign_fs mode.
    Uses pre-defined paths for key and specific files list, 
    which can be overriden
    :exits: status success/failure of the original script
    '''

    args = configure_args()

    script_main = os.path.join(SCRIPT_DIR, "sign_fs", "sign_main.py")
    cmd = [script_main, "-m", "sign_fs", "-k", args.key_path,
           "-p", args.path_to_sign]

    # handle multiple specific files
    if args.specific_files_list_path:
        for item in args.specific_files_list_path:
            cmd.append("-s")
            cmd.append(item)

    if args.force:
        cmd.append("-f")

    if args.extended_signature:
        cmd.append("--extended_signature")

    if args.include_absolute_path_in_signature:
        cmd.append("--include_absolute_path_in_signature")

    if args.no_specific_files:
        cmd.append("-n")

    try:
        retcode = call(cmd)
    except OSError as e:
        print("Execution failed:", e, stderr)
        retcode = 1
    exit(retcode)


if __name__ == "__main__":
    main()
