#!/bin/sh
#this script displayes the start/end time of boot and login procedures
grep "Command line: BOOT_IMAGE=" /var/log/syslog | tail -1 #start of boot
grep "manager: startup complete" /var/log/syslog | tail -1 #end of boot
grep "Successfully activated service 'org.freedesktop.systemd1'" /var/log/syslog | tail -1 #start login
grep "Entering running state" /var/log/syslog | tail -1 #end login
