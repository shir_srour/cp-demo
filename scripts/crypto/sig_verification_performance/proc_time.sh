#!/bin/sh
#calc time of starting a process
#default is "/usr/lib/gcc/x86_64-linux-gnu/6/cc1" (large proc).

# to test a different command, run:
#   export CMD="{your command}"
# and then run this script

export CMD="${CMD:-/usr/lib/gcc/x86_64-linux-gnu/6/cc1 --help}"
bash -c 'time ($CMD) > /dev/null' 2>&1 | grep real