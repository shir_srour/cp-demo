#! /bin/bash


# (C) Copyright Argus Cyber Security Ltd
# All rights reserved


# @(#) Bash script that parses csv file with hashes and generates
# csv formatted file with signatures using openssl rsautl signing
# Usage: $0 <name of csv file with hashes> <private key file, without passphrase>


# Prints usage line
function usage()
{
    echo "Usage: $0 <path to csv file with hashes> <private key file, without passphrase> <path to output file>"
}

# Checks if a string parameter is empty
# returns 1 if empty, 0 if not
function check_parameter()
{
    [ -z "${!1}" ] && echo "No $1 provided" && return 1

    return 0
}

# Checks if script input parameters are provided
# all parameters are required
# exits with exitcode 1 if one of the parameters is not provided
# returns 0 if all parameters were evaluated
function check_input()
{
    check_parameter path_to_csv
    local fail_csv=$?
    check_parameter key_path
    local fail_key=$?
    check_parameter out_path 
    local fail_out=$?

    [ $fail_csv -eq 0 -a $fail_key -eq 0 -a $fail_out -eq 0 ] && return 0

    usage && exit 1
}

# Makes an ASN1 config file using ecxtracted hexdigest:
# Adds padding to hexdigest in format of ASN.1 
# "algid = OID:2.16.840.1.101.3.4.2.1" is the SHA256 OID, 
# see http://oid-info.com/get/2.16.840.1.101.3.4.2.1
function asn_conf_eval()
{
    tee /tmp/asn1.conf >/dev/null <<-EOF
asn1 = SEQUENCE:digest_info_and_digest

[digest_info_and_digest]
dinfo = SEQUENCE:digest_info
digest = FORMAT:HEX,OCT:$1

[digest_info]
algid = OID:2.16.840.1.101.3.4.2.1
params = NULL
EOF
}

# 1. Parses the input csv file, do extract hexdigest:
#    1.1 Reads first line of csv file fieldnames and redirects it to new file
#    1.2 Reads the entire contents of the file, except first two lines
#    1.3 Reads each value in line, values are separated by IFS 
#        and extracts hexdigest
# 2. Makes an ASN1 config file using ecxtracted hexdigest -> heredoc
# 3. Makes a DER encoded ASN1 structure that contains the hash and
#    the hash type
# 4. Makes a signature file that contains both the ASN1 structure and
#    its signature
# 5. Removes the temporary files
function generate_csv_with_signatures()
{
    local path_to_csv="$1"
    local key_path="$2"
    local out_path="$3"

    head -n 1 ${path_to_csv} > $out_path
    tail -n +2 ${path_to_csv} | while IFS=, read -r path hexdigest sig
    do
        local DIGEST=$hexdigest
        asn_conf_eval $DIGEST
        openssl asn1parse -i -genconf /tmp/asn1.conf -out /tmp/data_file.dgst.asn1 -noout 
        signature=$(openssl rsautl -sign -in /tmp/data_file.dgst.asn1 -inkey $key_path | base64 -w 0)
        echo "$path,$hexdigest,$signature" >> $out_path
    done
    rm /tmp/asn1.conf
    rm /tmp/data_file.dgst.asn1
}

function main()
{
    local path_to_csv="$1"
    local key_path="$2"
    local out_path="$3"
    check_input
    generate_csv_with_signatures $path_to_csv $key_path $out_path
}

main $@
