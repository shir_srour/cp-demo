#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

from py_compile import compile
from shutil import copyfile
import tarfile

SIGN_FS_SCRIPTS     = ["analyze_paths_flow", "shadow_hash", "sign_main",
                       "deploy_signatures_flow", "json_file_data_io",
                       "signature_as_ext_attr", "unsign_fs_flow",
                       "file_data_io", "file_data_io_factory", "csv_file_data_io",
                       "pack_sign_fs_for_customer", "signature_delivery",
                       "verify_signatures_flow", "generate_signatures_flow"]
PY_EXTENSION        = ".py"
PYC_EXTENTION       = ".pyc"
REQUIREMENTS        = "requirements.txt"
CUSTOMER_SUFFIX     = "_customer"
README              = "README{suffix}.md".format(suffix=CUSTOMER_SUFFIX)
SIGN_FS_SCRIPT_PYC  = "sign_main.pyc"
GENERATE_SCRIPT     = "generate_signatures_csv.sh"
EXEC_PERMISSIONS    = 0755
READ_PERMISSIONS    = 0644
PACKAGE_FILE        = "sign_fs.tar.gz"


def set_permissions(tarinfo):
    '''
    Set permissions upon insertung files into archive

    :param tarinfo: tarinfo object for the current insertion
                        (implicit argument)
    :return: updated tarinfo object
    '''

    if tarinfo.name in [SIGN_FS_SCRIPT_PYC, GENERATE_SCRIPT]:
        tarinfo.mode = EXEC_PERMISSIONS
        return tarinfo
    else:
        tarinfo.mode = READ_PERMISSIONS
        return tarinfo


def main():
    package = tarfile.open(PACKAGE_FILE, mode='w:gz')

    # compile to *.pyc and pack into *.tar.gz archive
    for sign_fs_script in SIGN_FS_SCRIPTS:
        compile(sign_fs_script + PY_EXTENSION)
        file_name = sign_fs_script + PYC_EXTENTION
        package.add(file_name, arcname=file_name, filter=set_permissions)

    # pack general files into *.tar.gz archive
    for file_name in [README, REQUIREMENTS, GENERATE_SCRIPT]:
        package.add(
            file_name, arcname=file_name.replace(CUSTOMER_SUFFIX, ""),
            filter=set_permissions)
    package.close()


if __name__ == "__main__":
    main()
