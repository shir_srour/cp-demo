#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import subprocess

from signature_as_ext_attr import SignatureAsExtendedAttribute

from builtins import (dict, bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


def is_unique_digest_and_8_bytes(file_info,
                                 unique_bytes_set, unique_digest_set):
        '''
        Checks if the first 8 bytes of the signature are unique.
        Compares eleven 6-bit Base64 digits.
        (see https://en.wikipedia.org/wiki/Base64 for more info:
            "Each Base64 digit represents exactly 6 bits of data.
            Three 8-bit bytes (i.e., a total of 24 bits) can, therefore,
            be represented by four 6-bit Base64 digits.")

        :param file_info: file information as dictionary
                {"path": path/to/file, "digest": "XXXX", "signature": "YYYY"}
        :type: dict
        :return: result of the unique_check
        :type: bool
        '''
        FIRST_8_BYTES_IN_BASE64 = 11
        signature_8_bytes = file_info["signature"][:FIRST_8_BYTES_IN_BASE64]
        digest = file_info["digest"]

        unique = True

        if signature_8_bytes not in unique_bytes_set:
            unique_bytes_set.add(signature_8_bytes)
            unique_digest_set.add(digest)
        elif digest not in unique_digest_set and signature_8_bytes in unique_bytes_set:
            unique = False
        else:
            pass

        return unique


class DeploySignatures(object):
    '''
    Implements the deploy signatures flow
    '''

    def __init__(self, root_fs_path, files_info_iterable):
        '''
        :param root_fs_path: path to root_fs
        :type: str
        :param files_info_iterable:  list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        '''
        self.root_fs_path = root_fs_path
        self.files_info_iterable = files_info_iterable

    def deploy_signature(self, signature_storage, file_info):
        '''
        Deploys signature for file_path in file_info
            Normalizes the pathname by collapsing redundant separators
        :param signature_storage: SignatureAsExtendedAttribute instance
        :type: class instance
        :param file_info: file information as dictionary
                {"path": path/to/file, "digest": "XXXX", "signature": "YYYY"}
        :type: dict
        :return: result of set_signature
        :type: int
        '''
        return signature_storage.set_signature(os.path.normpath(
            os.path.join(
                self.root_fs_path, file_info["path"])), file_info["signature"])

    def main(self):
        '''
        Sets signatures for every file at the path specified
                            in files_info_iterable
        Counts collisions:
            count increased if 8 first bytes of the signature are not unique
        :return: True/False if no_collisions
        '''
        signature_storage = SignatureAsExtendedAttribute()
        unique_bytes_set = set()
        unique_digest_set = set()
        failed_deploy = []
        failed_collisions = []
        successes = 0

        for file_info in self.files_info_iterable:
            # set signature and check result
            deploy_res = self.deploy_signature(
                signature_storage, file_info)
            if (deploy_res != 0):
                failed_deploy.append(file_info["path"])
            else:
                successes += 1

            # check for collisions
            collisions_res = is_unique_digest_and_8_bytes(
                file_info, unique_bytes_set, unique_digest_set)
            if not collisions_res:
                failed_collisions.append(file_info["path"])

        return {"successes": successes,
                "failed_deploy": failed_deploy,
                "failed_collisions": failed_collisions}
