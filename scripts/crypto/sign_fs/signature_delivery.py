"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import abc


class SignatureDelivery(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_signature(self, file_path=None):
        """
        Get signature from the file indicated by file_path
        :param file_path: path to file to get signature
        :type: str
        :return: retrieved signature
        :type: str
        """
        raise NotImplementedError('subclasses of SignatureDelivery \
            must provide a get_signature() method')

    @abc.abstractmethod
    def set_signature(self, file_path=None, value=None):
        """
        Set signature of provided value the to the file indicated by file_path
        :param file_path: path to file to set signature to
        :type: str
        :param value: value of signature
        :type: str
        :return: None
        """
        raise NotImplementedError('subclasses of SignatureDelivery \
            must provide a set_signature() method')

    @abc.abstractmethod
    def unset_signature(self, file_path=None):
        """
        Unset signature from the file indicated by file_path
        :param file_path: path to file to remove signature from
        :type: str
        :return: None
        """
        raise NotImplementedError('subclasses of SignatureDelivery \
            must provide a get_signature() method')
