
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import csv
from file_data_io import FileDataIO


class CSVFileDataIO(FileDataIO):
    '''
    Implements reading/writing data for CSV type file_objects
    '''

    def __init__(self, file_object):
        '''
        Initializes CSVFileDataIO object
        :param file_object: opened python file object with r/w permissions
        :type: file
        '''
        self.file_object = file_object

    def read(self):
        '''
        Deserialize file_object to a python string and then
        the resulting string to a python iterable object.
        :return: list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
            Does NOT deal with inappropriate file types!
        '''
        csv_reader = csv.DictReader(
            self.file_object)
        files_info = [row for row in csv_reader]
        return files_info

    def write(self, files_info):
        '''
        Serialize files_info obj to a python string object.
        Then serialize the resulting string
        as a CSV formatted stream to file_object.
        Omits entirely dictionary keys
        :param files_info: list of dictionaries, each of form:
                    {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        :return: NoneType.
                 Writes data to the provided file_path in csv format.
        :throws: TypeError - when applied to a file_object of
                    inappropriate type
        '''
        keys = files_info[0].keys()
        csv_writer = csv.DictWriter(self.file_object, keys)
        csv_writer.writeheader()
        csv_writer.writerows(files_info)
