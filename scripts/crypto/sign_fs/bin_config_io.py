#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved

This script converts a yaml file to binary config file for ASE.
Usage: yaml_conf_to_bin.py <YAML_FILE> <OUTPUT_BINARY_FILE>
The binary is written in big-endian order with integers written as int64 and
consists of Type - Length - Value records.
Type - is enum number identifying the specific param,
length is the length of the value, value - the value.
Arrays of strings are have recursive TLV structure.

For example, the following yaml configuration :

version: 5
ARGUS_CONFIG_IS_MONITOR: 1
ARGUS_CONFIG_ARGUS_FILES_PATHS:
    - "/sbin/auditd"
    - "/sbin/audispd"


Will look like this:

   version
| 00 00 00 05 |

  is_monitor     int length         1
| 00 00 00 00 | 00 00 00 08 | 00 00 00 00 00 00 00 01 |

  file_paths    total length of all subsequent sub TLV records
| 00 00 00 0e | 00 00 00 29 |

  arr elm type    length        /sbin/auditd
| 0f ff ff bb | 00 00 00 0c | 2f 73 62 69 6e 2f 61 75 64 69 74 64

  arr elm type    length        /sbin/audispd
| 0f ff ff bb | 00 00 00 0d | 2f 73 62 69 6e 2f 61 75 64 69 73 70 64

"""

from __future__ import unicode_literals
import os
import struct
from functools import partial

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from past.builtins import (basestring, long)

from future import standard_library
standard_library.install_aliases()

ARR_ELM_TYPE = 0x0fffffbb


def read_n(n, fmt, binary):
    """
    Reads n bytes from a binary stream in big-endian order.
    :param n: Number of bytes to read.
    :param fmt: Format with which the read bytes are unpacked (struct.unpack).
    :param binary: A File opened as binary.
    :return: The read value, according to the given format.
    :raise EOFError: If given file reached EOF.
    :raise RuntimeError: If there are less bytes in the given file than n.
    """
    n_bytes = binary.read(n)
    if not n_bytes:
        raise EOFError()

    # Encountered problem in binary
    if len(n_bytes) != n:
        raise RuntimeError("Tried to read {} bytes from binary, "
                           "but only {} bytes read".format(
                               n, len(n_bytes)))

    return struct.unpack(fmt, n_bytes)[0]


# See https://docs.python.org/2/library/struct.html
read_4 = partial(read_n, 4, ">i")
read_8 = partial(read_n, 8, ">q")


def read_bool(binary):
    """
    Reads a boolean from the given binary file.
    :param binary: A File opened as binary.
    :return: A Boolean.
    """
    return read_int(binary) != 0


def read_int(binary):
    """
    Reads an integer from the given binary file.
    :param binary: A File opened as binary.
    :return: An int.
    """
    # Length is unused, int is always stored in 8 bytes.
    # See yaml_conf_to_bin.py::write_int.
    read_4(binary)
    return read_8(binary)


def read_str(binary):
    """
    Reads a string (array of bytes that is not null terminated).
    :param binary: A File opened as binary.
    :return: The read string.
    """
    length = read_4(binary)
    return read_n(length, '{0}s'.format(length), binary)


def read_arr_int(binary):
    """
    Reads an array of integers from the given file in big-endian order.
    :param binary: :param binary: A File opened as binary.
    :return: The read array of integers.
    """
    length = read_4(binary)
    return [read_8(binary) for _ in range(length)]


def read_arr_str(binary):
    """
    Reads an array of strings (array of bytes that is not null terminated)
        from the given file.
    :param binary: A File opened as binary.
    :return: The read array of strings.
    :raise RuntimeError: If binary contains an invalid array of strings.
    """
    str_arr = []
    length = read_4(binary)

    while length > 0:
        element_type = read_4(binary)
        if ARR_ELM_TYPE != element_type:
            raise RuntimeError(
                "Encountered invalid element type: {}".format(
                    element_type))
        string = read_str(binary)
        str_arr.append(string)
        length -= len(string) + 8

    if length < 0:
        raise RuntimeError(
            "Encountered invalid string of length {}".format(
                length))

    return str_arr


def is_int(value):
    """
    Check if the provided value is integer
    :param value: value to check
    :return: true if integer, false otherwise
    """
    if not isinstance(value, (int, long)):
        return False
    return True


def is_str(value):
    """
    Check if the provided value is string
    :param value: value to check
    :return: true if string, false otherwise
    """
    if not isinstance(value, basestring):
        return False
    return True


def is_int_arr(value):
    """
    Check if the provided value is array of integers
    :param value: value to check
    :return: true if array of integers, false otherwise
    """
    if not isinstance(value, list):
        return False
    return all(is_int(item) for item in value)


def is_str_arr(value):
    """
    Check if the provided value is array of integers
    :param value: value to check
    :return: true if array of integers, false otherwise
    """
    if not isinstance(value, list):
        return False
    return all(is_str(item) for item in value)


def write_1(value, binary):
    """
    Write 1 byte to binary stream
    :param value: byte to write
    :param binary: file opened as binary
    :return:
    """
    # 'c' means 1 byte
    fmt_byte = 'c'
    packed_value = struct.pack(fmt_byte, value)
    binary.write(packed_value)


def write_4(value, binary):
    """
    Write 4 bytes to binary stream in big-endian order
    :param value: 4 byte value to write
    :param binary: file opened as binary
    :return:
    """
    # '>i' means big-endian 4 bytes
    fmt_4bytes = '>i'
    packed_value = struct.pack(fmt_4bytes, value)
    binary.write(packed_value)


def write_8(value, binary):
    """
    Write 8 bytes to binary stream in big-endian order
    :param value: 8 byte value to write
    :param binary: file opened as binary
    :return:
    """
    # '>q' means big-endian 8 bytes
    fmt_8bytes = '>q'
    packed_value = struct.pack(fmt_8bytes, value)
    binary.write(packed_value)


def write_int(integer, binary):
    """
    Write integer and length (8) to binary stream in big-endian order
    :param integer: integer to write
    :param binary: file opened as binary
    :return:
    """
    # Length
    write_4(8, binary)
    # Value
    write_8(integer, binary)


def write_string(string, binary):
    """
    Write string (array of bytes that is not null terminated)
    and length (string length) to binary stream
    :param string: string to write
    :param binary: file opened as binary
    :return:
    """
    if len(string) == 0:
        raise Exception("Empty strings are not allowed")

    # string length
    write_4(len(string), binary)
    # Value
    string = string.encode('utf-8') \
        if isinstance(string, str) else string
    binary.write(string)


def write_arr_int(arr_int, binary):
    """
    Write array of integer and length (array size * 8) to binary stream
    in big-endian order
    :param arr_int: array of ints to write
    :param binary: file opened as binary
    :return:
    """
    # Length, arr length
    write_4(len(arr_int) * 8, binary)
    # Value
    for integer in arr_int:
        write_8(integer, binary)


def write_arr_str(arr_str, binary):
    """
    Write array of strings (array of array of bytes,
    each is not null terminated).
    Each string is a TLV record by itself with type = ARR_ELM_TYPE
    The length value in main TLV record is a total of
    all string sub TLV records
    :param arr_str: array of strings to write
    :param binary: file opened as binary
    :return:
    """
    # calculate length
    length = 0
    for string in arr_str:
        length += len(string)
        length += 8  # type + length fields

    # write length
    write_4(length, binary)
    # Value
    for string in arr_str:
        # add arr element type
        write_4(ARR_ELM_TYPE, binary)
        write_string(string, binary)


class BinConfig(object):
    """
    Implementation of read/write binary data recorded as TLV
    """
    TYPE_TO_READ_FUNC = {"BOOL": read_bool,
                         "INT": read_int,
                         "ARR_INT": read_arr_int,
                         "STR": read_str,
                         "BIN": read_str,
                         "ARR_STR": read_arr_str,
                         "ARR_BIN": read_arr_str}

    def __init__(self, bin_file_object):
        """
        :member bin_file_obj: path to binary file open for r/w
        """
        self.bin_file_obj = bin_file_object

    def __write_contents_per_type(self, yaml_config,
                                  name_to_enum_and_type):
        """
        Iterate over yaml configuration and save all the parameters
            in binary format
        :param yaml_config: yaml represented as map, vithout 'version' key
        :param name_to_enum_and_type: map of {enum names: (values, types)}
        :return: None
        """

        for yaml_param_name, yaml_value in yaml_config.items():
            if yaml_param_name in name_to_enum_and_type:
                try:
                    enum_value, enum_type = name_to_enum_and_type[
                        yaml_param_name]
                    # enum number --> Type
                    write_4(enum_value, self.bin_file_obj)

                    # Act according to yaml_value type
                    if ((enum_type == "INT" or enum_type == "BOOL") and
                            is_int(yaml_value)):
                        write_int(yaml_value, self.bin_file_obj)

                    elif ((enum_type == "STR" or enum_type == "BIN") and
                            is_str(yaml_value)):
                        write_string(yaml_value, self.bin_file_obj)

                    elif (enum_type == "ARR_INT" and is_int_arr(yaml_value)):
                        write_arr_int(yaml_value, self.bin_file_obj)

                    elif ((enum_type == "ARR_STR" or enum_type == "ARR_BIN") and
                            is_str_arr(yaml_value)):
                        write_arr_str((yaml_value), self.bin_file_obj)

                    else:
                        raise ValueError(
                            "The type of yaml_value '{}' "
                            "does not match the "
                            "actual type '{}'".format(
                                yaml_value, enum_type))
                except Exception as ex:
                    raise Exception(
                        "Error using parameter: {}".format(
                            yaml_param_name), ex)
            else:
                raise ValueError(
                    "Unsupported yaml_param_name '{}' "
                    "yaml_param_name does not match any of the "
                    "names in header files.".format(yaml_param_name))

    def write(self, yaml_config, version, enum_to_name_and_type_dict):
        """
        For an open binary file in write mode:
        Writes the contents of yaml_config in binary form
        :param yaml_config: yaml represented as map, vithout 'version' key
        :param version: version key
        :param enum_to_name_and_type_dict: map of {enum values: (names, types)}
        :param bin_file: path to output binary file
        :return: None
        """

        write_4(version, self.bin_file_obj)

        name_to_enum_and_type = {value[0]: (key, value[1])
                                 for key, value in
                                 enum_to_name_and_type_dict.items()}

        self.__write_contents_per_type(yaml_config, name_to_enum_and_type)

    def read(self, enum_to_name_and_type_dict):
        """
        Reads bytes from the given binary file, and translates them to a YAML
            that contains the names and values,
            according to the given header files.
        :param enum_to_name_and_type_dict: map of {enum values: (names, types)}
        :return: python dict
        """
        yaml_config = {}

        yaml_config['version'] = read_4(self.bin_file_obj)

        while True:
            # Read enum number of next param
            try:
                param_enum = read_4(self.bin_file_obj)
            except EOFError:
                # Configuration file has ended
                break

            param_name, param_type = enum_to_name_and_type_dict[param_enum]
            param_value = self.TYPE_TO_READ_FUNC[param_type](self.bin_file_obj)
            yaml_config[param_name] = param_value

        return yaml_config
