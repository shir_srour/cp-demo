#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import argparse
import logging

from analyze_paths_flow import AnalyzeFS
from generate_signatures_flow import GenerateSignatures
from deploy_signatures_flow import DeploySignatures
from file_data_io_factory import FileDataIOFactory
from unsign_fs_flow import UnsignFS
from verify_signatures_flow import VerifySignatures

from enum_parser import EnumParser
from bin_config_io import BinConfig

SUCCESS = 0
FAILURE = 1

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
ARGUS_LSM_DIR = os.path.dirname(os.path.abspath(
    os.path.join(__file__, "../../scripts/config")))
ARGUS_CONFIG_HEADER_PATH = os.path.realpath(os.path.join(
    ARGUS_LSM_DIR, "argus_config.h"))
ARGUS_CONFIG_BIN_PATH = os.path.realpath(os.path.join(
    ARGUS_LSM_DIR, "device_config/ase.config"))
#  Ready for when CUSTOMER_CONFIG is going to function
# CUSTOMER_CONFIG_HEADER_PATH = os.path.realpath(os.path.join(
#     ARGUS_LSM_DIR, "ase/src/config/customer_config.h"))
# CUSTOMER_CONFIG_BIN_PATH = os.path.realpath(os.path.join(
#     ARGUS_LSM_DIR, "dynamic_config/development/customer.config"))
ARGUS_KEYS = ["ARGUS_CONFIG_PATHS_TO_VERIFY_SIGNATURE"]
CUSTOMER_KEYS = [""]

logger = logging.getLogger('sign_fs_logger')


def configure_args():
    '''
    Parses command line arguments to specify the flow
    :param[in] mode: analyze - export hash data for appropriate files
                     deploy - import and deploy signatures
                     generate - generate signatures
                     sign_fs - analyze --> generate --> deploy
                     unsign - remove all signatures
                     verify - verify deployed signatures by simulating lsm
    :param[in] path_to_root_fs: directory tree or a regular file to sign
    :param[in] force: flag to indicate that specified path is to pe processed
    :param[in] extended_signature: flag to indicate files to be signed
                                   with advanced options
    :param[in] include_absolute_path_in_signature: flag to indicate file's
               content to be signed with its absolute path,
               relevant only if --extended_signature is set
    :param[in] no_specific_files: flag to indicate that no specific files
                                  are provided
    :param[in] specific_files_list_path: path to specific_files_list
    :param[in] input_sign_data_file_path: path to file with signatures for
               generate/deploy flow
    :param[in] output_sign_data_file_path: path to file to be filled
               for analyze/generate flow
    :param[in] key_path: path to file with key for generate flow
    :param[in] certificate_path: path to file with certificate for verify flow
    :param[in] file_type: input/output file types supported by script
                    the selection is limited to: csv or json
    :type[in]: str
    :return: parsed command line arguments
    '''
    parser = argparse.ArgumentParser(
        description="Import Signatures Flow",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-m", "--mode", default='sign_fs',
                        const='sign_fs', nargs='?',
                        choices=("analyze", "deploy", "generate",
                                 "sign_fs", "unsign", "verify"),
                        help="certificate file path ", type=str)
    parser.add_argument("-p", "--path_to_root_fs", type=str,
                        help="rootfs path to sign, or a file")
    parser.add_argument("-f", "--force", action='store_true',
                        default=False, help="force sign provided path")
    parser.add_argument("--extended_signature", action='store_true',
                        default=False, help="sign with advanced options")
    parser.add_argument("--include_absolute_path_in_signature",
                        action='store_true', default=False,
                        help="sign file's content and its path, "
                             "relevant only if --extended_signature is set")
    parser.add_argument("-n", "--no_specific_files", action='store_true',
                        default=False,
                        help="do not sign files from specific_file_list")
    parser.add_argument("-s", "--specific_files_list_path",
                        action='append', default=None, type=str,
                        help="sign files from specific_file_list, "
                             "multiple files can be provided")
    parser.add_argument("-i", "--input_sign_data_file_path",
                        default=None, type=str,
                        help="import signatures from the file specified")
    parser.add_argument("-o", "--output_sign_data_file_path",
                        default=None, type=str,
                        help="file to write results of analysis into")
    parser.add_argument("-b", "--use_binary", action='store_true',
                        default=False, help="use binary files to get specific"
                        " files lists")
    parser.add_argument("--argus_conf_bin_path",
                        default=None, type=str,
                        help="argus_config binary bile path")
    parser.add_argument("--argus_conf_h_path",
                        default=None, type=str,
                        help="argus_config.h header file path")
    #  Ready for when CUSTOMER_CONFIG is going to function
    # parser.add_argument("--customer_conf_bin_path",
    #                     default=os.path.realpath(os.path.join(
    #                         SCRIPT_DIR, CUSTOMER_CONFIG_BIN_PATH)), type=str,
    #                     help="customer_config binary bile path")
    # parser.add_argument("--customer_conf_h_path",
    #                     default=os.path.realpath(os.path.join(
    #                         SCRIPT_DIR, CUSTOMER_CONFIG_HEADER_PATH)), type=str,
    #                     help="customer_config.h header file path")
    parser.add_argument("-c", "--certificate_path",
                        default=None, type=str,
                        help="certificate file path")
    parser.add_argument("-k", "--key_path", help="key file path",
                        default=None, type=str)
    parser.add_argument("-t", "--file_type", nargs='?',
                        choices=("csv", "json"),
                        help="type of input/output file ",
                        default=None, type=str)

    args = parser.parse_args()
    if args.use_binary:
        if not args.argus_conf_bin_path:
            args.argus_conf_bin_path = os.path.realpath(
                os.path.join(SCRIPT_DIR, ARGUS_CONFIG_BIN_PATH))
        if not args.argus_conf_h_path:
            args.argus_conf_h_path = os.path.realpath(
                os.path.join(SCRIPT_DIR, ARGUS_CONFIG_HEADER_PATH))
    else:
        args.argus_conf_bin_path = None
        args.argus_conf_h_path = None
    return args


def init_logger():
    logging.basicConfig(
        level=logging.DEBUG, format='%(message)s')
    logger.propagate = False
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    logger.addHandler(ch)


def check_file_type_exists(file_type):
    '''
    Verifies the input/output file type is provided.
    :param key_path: path to key file
    :raises Exception: error message in case the paths is empty
    '''
    if not file_type:
        raise Exception(
            """No file type for input/output file supplied""".format(
                file_type))


def check_key_file_path(key_path):
    '''
    Verifies the key_path is provided.
    :param key_path: path to key file
    :raises Exception: error message in case the paths is empty
    '''
    if not key_path:
        raise Exception(
            """Invalid RSA_PRIVATE_KEY_PATH '{}' supplied""".format(
                key_path))
    elif not os.path.isfile(key_path):
        raise Exception(
            """Invalid RSA_PRIVATE_KEY_PATH '{}' supplied""".format(
                key_path))


def check_certificate_file_path(certificate_path):
    '''
    Verifies the certificate_file_path is provided.
    :param certificate_path: path to certificate file
    :raises Exception: error message in case the paths is empty
    '''
    if not certificate_path:
        raise Exception(
            """Invalid RSA_CERTIFICATE_PATH '{}' supplied""".format(
                certificate_path))
    elif not os.path.isfile(certificate_path):
        raise Exception(
            """Invalid RSA_CERTIFICATE_PATH '{}' supplied""".format(
                certificate_path))


def check_input_data_file(input_sign_data_file_path):
    '''
    Verifies the input_sign_data_file_path is provided.
    :param input_sign_data_file_path: path to input_sign_data_file
    :raises Exception: error message in case the paths is empty
    '''
    if not input_sign_data_file_path:
        raise Exception(
            "Invalid input_file '{}' supplied".format(
                input_sign_data_file_path))
    elif not os.path.isfile(input_sign_data_file_path):
        raise Exception(
            "Invalid input_file '{}' supplied".format(
                input_sign_data_file_path))


def check_path_to_root_fs_exists(path_to_root_fs):
    '''
    Verifies the path_to_root_fs is provided.
    :param path_to_root_fs: path to sign
    :raises Exception: error message in case the paths is empty
    '''
    if not os.path.exists(path_to_root_fs):
        raise Exception(
            "Invalid path to sign '{}' supplied".format(
                path_to_root_fs))


def check_extended_signature(extended_signature,
                             include_absolute_path_in_signature):
    '''
    Verifies --extended_signature is set if flags used in conjunction with it are set.
    :param extended_signature: flag to indicate files to be signed
                               with advanced options
    :param include_absolute_path_in_signature: flag to indicate file's content
                                               to be signed with its absolute path
    :raises Exception: error message in case --extended_signature
                       is not set when it should
    '''
    if not extended_signature:
        if include_absolute_path_in_signature:
            raise Exception("--extended_signature must be set "
                            "in order to set --include_absolute_path_in_signature")


def check_forced_path_to_sign(path_to_root_fs, force):
    '''
    Verifies the path_to_root_fs is provided.
    :param path_to_root_fs: path to sign
    :raises Exception: error message in case the paths is empty
    '''
    try:
        if (not os.path.isdir(path_to_root_fs) and
                not os.path.isfile(path_to_root_fs)):
            raise Exception("Invalid path to sign. "
                            "'{}' is not a directory and "
                            "not a regular file.".format(path_to_root_fs))
        elif (os.path.isfile(path_to_root_fs) and not force):
            raise Exception("Invalid path to sign. "
                            "'{}' is not a directory and "
                            "not forced mode.".format(path_to_root_fs))
    except TypeError:
        raise Exception(
            "Invalid path to sign '{}' supplied".format(path_to_root_fs))


def check_config_analyze_mode(args):
    '''
    Verify command line arguments needed for the analyze flow.
    Required arguments are:
        path_to_root_fs: provided path_to_root_fs
        specific_files_list_path: path to specific_files_list_path
        force: flag to indicate an intention to sign an unorthodox path
        no_specific_files: flag to indicate an intention to not sign
                    the specific_files_list_path
        file_type: supported output file format {csv, json}
    :param args: command line arguments
    :raises Exception: error message in case required arguments are missing
    '''

    check_file_type_exists(args.file_type)
    check_path_to_root_fs_exists(args.path_to_root_fs)
    check_forced_path_to_sign(args.path_to_root_fs, args.force)
    check_extended_signature(args.extended_signature,
                             args.include_absolute_path_in_signature)


def check_config_generate_mode(args):
    '''
    Verify command line arguments needed for the generate flow.
    Required arguments are:
        input_sign_data_file_path: provided path to
                        input_sign_data_file_path
        key_path: provided file path for private key
        file_type: supported input and output file format {csv, json}
    :param args: command line arguments
    :raises Exception: error message in case on of the paths is empty
    '''
    check_input_data_file(args.input_sign_data_file_path)
    check_file_type_exists(args.file_type)
    check_key_file_path(args.key_path)


def check_config_deploy_mode(args):
    '''
    Verify command line arguments needed for the deploy flow.
    Required arguments are:
        path_to_root_fs: provided path_to_root_fs, so that
            the pathes in input_sign_data_file would be relative
            to the provided path_to_root_fs
        input_sign_data_file_path: provided
                                    input_sign_data_file_path
        file_type: supported input file format {csv, json}
    :param args: command line arguments
    :raises Exception: error message in case on of the paths is empty
    '''

    check_path_to_root_fs_exists(args.path_to_root_fs)

    if not os.path.isdir(args.path_to_root_fs):
        raise Exception(
            "Path to sign must be the root_fs directory. "
            "Invalid path '{}' supplied".format(
                args.path_to_root_fs))

    check_input_data_file(args.input_sign_data_file_path)
    check_file_type_exists(args.file_type)


def check_config_verify_mode(args):
    '''
    Verify command line arguments needed for the verify flow.
    Required arguments are:
        path_to_root_fs: provided path_to_root_fs
        certificate_path: configured certificate path
        specific_files_list_path: configured specific_list path
        force: flag to indicate an intention to sign
                            an unorthodox path
        no_specific_files: flag to indicate an intention to
                            not sign the specific_file_list
    :param args: command line arguments
    :raises Exception: error message in case required arguments are missing
    '''
    check_path_to_root_fs_exists(args.path_to_root_fs)
    check_forced_path_to_sign(args.path_to_root_fs, args.force)
    check_certificate_file_path(args.certificate_path)


def check_config_unsign_mode(args):
    '''
    Verify command line arguments needed for the unsign flow.
    Required argument is:
        path_to_root_fs: provided path_to_root_fs
    :raises Exception: error message in case required arguments are missing:
    '''
    check_path_to_root_fs_exists(args.path_to_root_fs)


def check_config_sign_fs(args):
    '''
    Verify command line arguments needed for the sign_fs flow.
    Required arguments are (covered in check_config_analyze_mode):
        path_to_root_fs: provided path_to_root_fs
        specific_files_list_path: path to specific_files_list_path
        force: flag to indicate an intention to sign an unorthodox path
        no_specific_files: flag to indicate an intention to not sign
                    the specific_files_list_path
        Checked here is:
        key_path: provided file path for private key
    :param args: command line arguments
    :raises Exception: error message in case required arguments are missing
    '''
    check_path_to_root_fs_exists(args.path_to_root_fs)
    check_forced_path_to_sign(args.path_to_root_fs, args.force)
    check_key_file_path(args.key_path)
    check_extended_signature(args.extended_signature,
                             args.include_absolute_path_in_signature)


def make_specific_files_list_txt(specific_files_list_path):
    '''
    Converts contents of a specific_files_list file to a list
    :param specific_files_list_path: path to specific_files_list_path
    :type: str
    :return: list of specific files or empty list
    :type: list of str
    '''
    specific_files_list = []

    for path in specific_files_list_path:
        with open(path, 'r') as f:
            content = [line.strip() for line in f]

        specific_files_list.extend(content)

    return specific_files_list


def get_header_params_data(config_header_file):
    """
    Creates a mapping between the enum number,
    the enum name and the type of the config param, which is declared
    in the given config_header_file.
    :param config_header_file: A file
        containing the type declarations of config params.
    :return: A dictionary of ints to tuples of (string, string), e.g.
    {0: ("ARGUS_CONFIG_IS_MONITOR", "INT")...}
    :raise RuntimeError: If the param names from enum in the header file
        do not match the param names in the header file.
    """
    with open(config_header_file, "r") as config_params_header:
        enum_parser = EnumParser(config_params_header)
        return enum_parser.enum_to_name_and_type_dict()


def make_specific_files_list_from_bin(
        specific_files_list_path, config_header_file, keys_list):
    '''
    Converts contents of a specific_files_list file to a list
    :param specific_files_list_path: path to specific_files_list_path
    :type: str
    :return: list of specific files or empty list
    :type: list of str
    '''
    specific_files_list = []

    with open(specific_files_list_path, 'rb') as binary:
        enum_to_name_and_type_dict = get_header_params_data(
            config_header_file)
        content_dict = BinConfig(binary).read(
            enum_to_name_and_type_dict)

    for key in keys_list:
        values = content_dict[key]
        specific_files_list.extend([v.decode("utf-8") for v in values])

    return specific_files_list


def write_files_info_to_file(file_out_path, file_type, files_info):
    '''
    Writes the contents of file_info object into the file
        located at file_out_path
    :param file_out_path:
    :type: str
    :param files_info:
    :type: list object with relevant files info
    :return: None
    '''

    with open(file_out_path, "w") as file_out:
        file_data_io = FileDataIOFactory().get_file_data_io(
            file_type, file_out)
        file_data_io.write(files_info)


def read_files_info_from_file(file_type, file_in_path):
    '''
    Reads the contents form the file
        located at file_in_path
    :param file_in_path:
    :type: str
    :return: list object with relevant files info
    :type: list
    '''

    with open(file_in_path, "r") as file_in:
        file_data_io = FileDataIOFactory().get_file_data_io(
            file_type, file_in)
        return file_data_io.read()


def get_path_to_root_fs(args):
    '''
    Sets path_to_root_fs to empty string if
        provided path is to a file
    :param args: command line arguments
    :return: path to root_fs
    :type: str
    '''
    if os.path.isfile(args.path_to_root_fs):
        return ""
    return args.path_to_root_fs


def get_specific_files_list(args):
    '''
    Creates specific files list.
    If provided path was to a single file, the file is added
    as part of specific files list
    :param args: command line arguments
    :return: list of specific files paths
    :type: list
    '''
    specific_files_list = []
    if os.path.isfile(args.path_to_root_fs):
        specific_files_list.extend([args.path_to_root_fs])
    if args.specific_files_list_path:
        specific_files_list.extend(
            make_specific_files_list_txt(args.specific_files_list_path))
    if (args.argus_conf_bin_path and args.argus_conf_h_path):
        specific_files_list.extend(
            make_specific_files_list_from_bin(
                args.argus_conf_bin_path, args.argus_conf_h_path,
                ARGUS_KEYS))
    #  Ready for when CUSTOMER_CONFIG is going to function
    # if (args.customer_conf_bin_path and args.customer_conf_h_path):
    #     specific_files_list.extend(
    #         make_specific_files_list_from_bin(
    #             args.customer_bin_path,
    #             args.customer_conf_h_path, CUSTOMER_KEYS))
    return specific_files_list


def log_deploy_summary(result):
    '''Log the signature deploy summary
    :param result: counted successes, a list of failed paths,
                    a list of collision paths
    :type: dict(int, list, list)
    :return: None
    '''
    logger.debug("\n Deploy Signatures Summary:")
    logger.debug("-- Signed %s files with %s collisions",
                 result["successes"], len(result["failed_collisions"]))
    if result["failed_collisions"]:
        for failed_path in result["failed_collisions"]:
            logger.debug("-- Collision in file: %s",
                         failed_path)
    if result["failed_deploy"]:
        logger.debug("-- Could not sign %s files",
                     len(result["failed_deploy"]))
        for failed_path in result["failed_paths"]:
            logger.debug("-- Signing failed for file: %s",
                         failed_path)


def log_verify_summary(result):
    '''
    Log the verification summary
    :param result: counted successes and a list of failed paths
    :type: dict(int, list)
    :return: None
    '''
    logger.debug("\n Verification Summary:")
    logger.debug("-- Signature verification succeeded for %s files",
                 result["successes"])
    if result["failed_paths"]:
        logger.debug("-- Signature verification failed for %s files",
                     len(result["failed_paths"]))
        for failed_path in result["failed_paths"]:
            logger.debug("-- Signature verification failed for file: %s",
                         failed_path)


def log_unsign_summary(result):
    '''Log the unsign summary
    :param result: counted successes and a list of failed paths
    :type: dict(int, list)
    :return: None
    '''
    logger.debug("\n Unsign Summary:")
    logger.debug("-- Signature unsign succeeded for %s files",
                 result["successes"])
    if result["failed_unsign"]:
        logger.debug("-- Signature unsign failed for %s files",
                     len(result["failed_unsign"]))
        for failed_path in result["failed_unsign"]:
            logger.debug(
                "-- No signature attribute is set for: %s", failed_path)


def run_analyze_flow(args):
    '''
    Runs the analyze flow:
        - Checks the correct config arguments
        - Creates an AnalyzeFS object that analyzes file system
            by the provided citeria
        - Writes the result of analysis into file
    If the path provided is for a concrete file and not for root_fs
    will add the path as part of specific files list
    :param args: command line arguments
    :return: None
    '''
    logger.debug("analyze")

    check_config_analyze_mode(args)
    path_to_root_fs = get_path_to_root_fs(args)
    specific_files_list = get_specific_files_list(args)
    analyze = AnalyzeFS(path_to_root_fs, specific_files_list,
                        args.force, args.extended_signature,
                        args.include_absolute_path_in_signature)

    files_info_iterable = analyze.main()

    write_files_info_to_file(
        args.output_sign_data_file_path, args.file_type, files_info_iterable)


def run_generate_flow(args):
    '''
    Runs the generate flow:
        - Checks the correct config arguments
        - Reads the files info from the provided file
        - Creates an GenerateSignatures object that generates
            signatures for every listed entry
        - Writes the result of generate process into file
    :param args: command line arguments
    :return: None
    '''
    logger.debug("generate")
    check_config_generate_mode(args)
    files_info_iterable = read_files_info_from_file(
        args.file_type, args.input_sign_data_file_path)

    generate = GenerateSignatures(files_info_iterable,
                                  args.key_path)
    files_info_signed = generate.main()

    write_files_info_to_file(
        args.output_sign_data_file_path, args.file_type, files_info_signed)


def run_deploy_flow(args):
    '''
    Runs the deploy flow:
        - Checks the correct config arguments
        - Reads the files info from the provided file
        - Creates a DeploySignatures object that signs the listed files,
                    and checks for collisions in signatures
    :param args: command line arguments
    :return: Success - if no collisions were detected
           : Failure - if collisions were detected
    '''
    logger.debug("deploy")
    check_config_deploy_mode(args)
    files_info_iterable = read_files_info_from_file(
        args.file_type, args.input_sign_data_file_path)

    deploy = DeploySignatures(args.path_to_root_fs, files_info_iterable)
    result_dict = deploy.main()
    log_deploy_summary(result_dict)

    if not result_dict["failed_collisions"] and not result_dict["failed_deploy"]:
        return SUCCESS
    return FAILURE


def sign_fs(args):
    '''
    Runs the sign_fs:
        - Checks the correct config arguments
        - Creates an AnalyzeFS object that analyzes file system
            by the provided citeria
        - Creates an GenerateSignatures object that
            generates signatures for every entry listed in the
            result of analysis from previous stage.
        - Creates an DeploySignatures object that signs the files
            listed in the result of previous stages
    :param args: command line arguments
    :return: Success - if no collisions were detected
           : Failure - if collisions were detected
    '''
    check_config_sign_fs(args)

    logger.debug("sign_fs: analyze")
    path_to_root_fs = get_path_to_root_fs(args)
    specific_files_list = get_specific_files_list(args)
    analyze = AnalyzeFS(path_to_root_fs, specific_files_list,
                        args.force, args.extended_signature,
                        args.include_absolute_path_in_signature)

    files_info_iterable = analyze.main()

    logger.debug("sign_fs: generate")
    generate = GenerateSignatures(files_info_iterable,
                                  args.key_path)
    files_info_signed = generate.main()

    logger.debug("sign_fs: deploy")
    deploy = DeploySignatures(path_to_root_fs, files_info_signed)
    result_dict = deploy.main()
    log_deploy_summary(result_dict)

    if not result_dict["failed_collisions"] and not result_dict["failed_deploy"]:
        return SUCCESS
    return FAILURE


def run_verify_flow(args):
    '''
    Runs the verify flow.
    :param args: command line arguments
    :return: None
    '''
    check_config_verify_mode(args)
    logger.debug("verify: analyze")
    path_to_root_fs = get_path_to_root_fs(args)
    specific_files_list = get_specific_files_list(args)
    analyze = AnalyzeFS(path_to_root_fs, specific_files_list,
                        args.force, args.extended_signature,
                        args.include_absolute_path_in_signature)

    files_info_iterable = analyze.main()

    logger.debug("verify: verify")
    verify = VerifySignatures(path_to_root_fs,
                              files_info_iterable, args.certificate_path)
    result_dict = verify.verify()
    log_verify_summary(result_dict)

    if result_dict["failed_paths"]:
        return FAILURE
    return SUCCESS


def run_unsign_flow(args):
    '''
    Checks the command line arguments according to the
            unsign flow requirements.
    Runs the unsign flow.
    :param args: command line arguments
    :return: None
    '''
    logger.debug("unsign")
    check_config_unsign_mode(args)
    unsign = UnsignFS(args.path_to_root_fs)
    res_dict = unsign.main()
    log_unsign_summary(res_dict)


def main():
    '''
    Calls run functions of the selected flow, according to
        command line arguments received
    :exits: status success/failure
    '''

    init_logger()

    if os.getuid() != 0:
        logger.error('script needs to run as root')
        return

    args = configure_args()
    result = SUCCESS

    try:
        if (args.mode == "analyze"):
            print("mode: analyze")
            run_analyze_flow(args)

        elif (args.mode == "generate"):
            print("mode: generate")
            run_generate_flow(args)

        elif (args.mode == "deploy"):
            print("mode: deploy")
            result = run_deploy_flow(args)

        elif (args.mode == "sign_fs"):
            print("mode: sign_fs")
            result = sign_fs(args)

        elif (args.mode == "unsign"):
            print("mode: unsign")
            run_unsign_flow(args)

        elif (args.mode == "verify"):
            print("mode: verify")
            result = run_verify_flow(args)
        else:
            raise ValueError("Mode unrecognized. Specify a correct mode")
    except Exception as e:
        print("{} process aborted.".format(args.mode))
        logger.error(e)
        result = FAILURE
    exit(result)


if __name__ == "__main__":
    main()
