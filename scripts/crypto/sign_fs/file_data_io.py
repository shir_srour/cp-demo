
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import abc


class FileDataIO(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def read(self, file_path=None):
        '''
        :param file_path: from where to read
        :type: str
        :return: list of dictionaries, each of form
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        '''
        raise NotImplementedError(
            'subclasses of FileDataIO must provide a read() method')

    @abc.abstractmethod
    def write(self, file_path, files_info):
        '''
        :param file_path: where to write into
        :type: str
        :param files_info: list of dictionaries, each of form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        return: NoneType
                writes data to the provided file_path in json format
        '''
        raise NotImplementedError(
            'subclasses of FileDataIO must provide a write() method')
