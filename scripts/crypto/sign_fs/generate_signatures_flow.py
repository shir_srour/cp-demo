#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import abc
import base64

from shadow_hash import ShadowHash

from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


class SignatureGenerator(object):
    '''
    Signature generator API
    '''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def generate_signature(self, *args, **kwargs):
        """
        Generate signature abstract class
        :param args: positional arguments
        :param kwargs: named arguments
        :return: None
        """
        raise NotImplementedError(
            "subclasses of SignatureDelivery must provide a\
                                generate_signature() method")


class PKCS1forRSA(SignatureGenerator):
    '''
    Generate signature with RSA public-key cryptosystem algorithm
    '''

    def __init__(self, key_file):
        '''
        Create a signature scheme object to be used for signature creation
        :param key_file: file opened for read with private RSA key object
        :type: str
        '''
        self.signer = PKCS1_v1_5.new(RSA.importKey(key_file.read()))

    def generate_signature(self, hex_digest):
        '''Generate signature for a given file
        :param hex_digest: pre calculated hash_value for
            generating signature in hex format
        :type: str
        :return: signature in bytes
        :type: str
        '''
        return self.signer.sign(ShadowHash(hex_digest))


class GenerateSignatures(object):
    '''
    Implements the generate signatures flow
    '''

    def __init__(self, files_info_iterable,
                 key_file_path):
        '''
        :param file_info_iterable:  list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY", signing_mode:'XX'}
                signing_mode example values:'\x00'
                                            '\x01'
                note: for files signed without extended signature (ase.ko)
                      signing_mode's value will be empty ('')
        :type: list
        :param key_file_path: path to file with relative path to file and
                            its signature for deployment process
        :type: str
        '''
        self.files_info_iterable = files_info_iterable
        self.private_key_file = key_file_path

    def main(self):
        '''
        Generates signature value for every file at the path specified
            in files_info_iterable
        Writes generated signature value into the output dictionary.
            Each value is mapped to path provided
        :return: list of dictionaries
        :type: list
        '''
        files_info_sig = []
        with open(self.private_key_file, 'rb') as key_file:
            signer = PKCS1forRSA(key_file)

            for item in self.files_info_iterable:
                bin_signature = signer.generate_signature(
                    item["digest"])
                bin_signature += item["signing_mode"].encode('utf-8')
                base64_signature = base64.b64encode(
                    bin_signature).decode('utf8')
                item["signature"] = base64_signature
                files_info_sig.append(item)

        return files_info_sig
