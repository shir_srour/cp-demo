#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import subprocess

from signature_delivery import SignatureDelivery

from builtins import (dict, bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


class SignatureAsExtendedAttribute(SignatureDelivery):
    '''
    Set/Get signature as extended attribute
    '''
    def __init__(self, sig_attr=None):
        '''
        :param sig_attr: signature attribute, as required by
                            extended attributes.
                         If none provided, defaults to predefined one
        :type: str
        '''
        self.sig_attr = sig_attr or "security.argus_sig"

    @property
    def sig_attr(self):
        return self.__sig_attr

    @sig_attr.setter
    def sig_attr(self, new_sig_attr):
        self.__sig_attr = new_sig_attr

    def set_attr_cmd(self, value, file_path):
        '''Creates a formatted set attribute command
        :param value: extended attribute value to set in base64 format
        :type: str
        :param file_path: path to file to set attributes to
        :type: str
        :return: formatted set attribute command
        :type: str
        '''
        return 'setfattr -n {} -v "{}" {}'.format(
            self.sig_attr, value, file_path)

    def get_attr_cmd(self, file_path):
        '''Creates a formatted get attribute command
        :param file_path: path to file to set attributes to
        :type: str
        :return: formatted get attribute command
        :type: str
        '''
        return 'getfattr -n {} --only-values --absolute-names {}'.format(
            self.sig_attr, file_path)

    def attr_exists(self, file_path):
        '''Checks if the named extended attribute exists for the provided file
        :param file_path: path to file to set attributes to
        :type: str
        :return: true if attr exists, False - otherwise
        :type: bool
        '''
        cmd = 'sudo getfattr -n {} {} 1>/dev/null 2>/dev/null'.format(
            self.sig_attr, file_path)
        return 0 == os.system(cmd)

    def remove_attr_cmd(self, file_path):
        '''Creates a formatted remove attribute command
        :param file_path: path to file to set attributes to
        :type: str
        :return: formatted change attribute command
        :type: str
        '''
        return 'sudo setfattr -x {} {}'.format(self.sig_attr, file_path)

    def get_signature(self, file_path):
        '''Create and run "get extended attributes" command with
            all the neccessary parameters
        :param file_path: path to file to retrieve attributes from
        :type: str
        :return: signature
        :throws: if the attribute is not set
        '''
        return subprocess.check_output(
                self.get_attr_cmd(file_path), shell=True)

    def set_signature(self, file_path, value):
        '''Adds the value as extended attribute signature to
            the specified file
            - Adds to value string the 0s or 0S, base64 encoding is expected
            - Removes immutable flag, on failure stops
            - Removes the signature as extended attribute
        :param file_path: file to unset attributes from
        :type: str
        :return: result of set_attr_cmd call
        '''
        value = '0s' + value
        return subprocess.call(
            self.set_attr_cmd(value, file_path), shell=True)

    def unset_signature(self, file_path):
        '''Removes the extended attribute signature from specified file
            - Checks if the attribute exists, if not stops
            - Removes the signature as extended attribute
        :param file_path: file to unset attributes from
        :type: str
        :return: result of the remove_attr_cmd command
        :throws: if the attribute is not set
        '''
        if not self.attr_exists(file_path):
            raise AttributeError(
                "No signature attribute set for path: {}".format(file_path))

        return os.system(self.remove_attr_cmd(file_path))
