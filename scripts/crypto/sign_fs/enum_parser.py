#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import re


class EnumParser(object):
    """
    Parses header files with enum and parameter types
    Maps the parameters name, value and type
    Public functions:
        * map_enum_config_types_and_values()
        :return: map of enum strings to enum numbers
        * map_param_config_types_to_enum()
        :return: map of enum names and param types
        * enum_to_name_and_type_dict()
        :return: map of enum numbers,
            to tuple of enum strings and types
    """
    ENUM_SUBSTR_REGEX = r"ARGUS_CONFIG_[A-Z0-9_]*"
    ENUM_START = "{"
    ENUM_END = "}"
    PARAM_NAME = "name"
    PARAM_TYPE = "type"
    PARAM_DECLARATION_RE = \
        re.compile(
            r"ARGUS_PARAM_(?P<{0}>\w*)_DECLARE_EXTERN\((?P<{1}>\w*)\)".format(
                PARAM_TYPE, PARAM_NAME))

    def __init__(self, config_header_file_object):
        """
        :param config_header_file_object: file object with enum value type
        :member enum_config_map: map of enum strings to enum numbers {str:int}
        :member param_config_map: map of enum names and param types {str:str}
        """
        self.config_header_file_object = config_header_file_object
        self.enum_config_map = None
        self.param_config_map = None

    def _get_enum_from_header(self):
        """
        Parses the header file that contains enum of config_types
            declarations that look like ENUM_SUBSTR_REGEX.
        Checks if the enum is present at all in the current file.
        :return: a string of enum contents for further mapping
        :type: str
        """
        self.config_header_file_object.seek(0)
        header_content = self.config_header_file_object.read()
        # sanity checks
        if (header_content.find(self.ENUM_START) == -1 or
                header_content.find(self.ENUM_END) == -1):
            raise ValueError("Invalid enum header file")
        else:
            # trim the string only to contain enum contents { ... }
            return header_content[header_content.find(
                    self.ENUM_START):header_content.find(self.ENUM_END)]

    def map_enum_config_types_and_values(self):
        """
        Maps the config_type names to the values
        :return: map of enum strings to enum numbers {str:int}
            example: {"ARGUS_CONFIG_IS_MONITOR": 0, ...}
        """
        if not self.enum_config_map:
            enums = re.findall(self.ENUM_SUBSTR_REGEX,
                               self._get_enum_from_header())
            if len(enums) < 1:
                raise ValueError("Invalid enum header file")

            enums = enums[0:-1]  # remove the last enum which is CONFIG_MAX

            self.enum_config_map = dict(zip(enums, range(0, len(enums))))
        return self.enum_config_map

    def _param_config_names_to_types(self):
        """
        Parses config param types from a header file that contains
            parameter declarations that look like PARAM_DECLARATION_RE.
        :param config_params_path: Path to a header file with
            param declarations.
        :return: A dict between param names and param types (both strings),
            example: {"is_monitor": "INT", ...}
        """
        self.config_header_file_object.seek(0)
        config_params_lines = self.config_header_file_object.readlines()

        names_to_types = {}
        for line in config_params_lines:
            match = re.match(self.PARAM_DECLARATION_RE, line)
            if match:
                match_dict = match.groupdict()
                names_to_types[
                    match_dict[self.PARAM_NAME]] = match_dict[self.PARAM_TYPE]

        return names_to_types

    def map_param_config_types_to_enum(self):
        """
        Changes the keys of param_types, which are names of the
        config variables in ASE, to the enum name of the config key.
        Basically, for every variable name the function adds
        "ARGUS_CONFIG_" prefixes and capitalizes them.
        :param param_types: A dict between param names and param types
        :type: str
        :return: A dict between param enum names and param types (both strings)
        :type: dict
        """
        if not self.param_config_map:
            self.param_config_map = \
                {"ARGUS_CONFIG_" + par_name.upper(): par_type
                 for par_name, par_type in
                 self._param_config_names_to_types().items()}
        return self.param_config_map

    def _compare_params_config_types(self, params_from_enum_header_key_set,
                                     param_types_key_set):
        """
        Compares parameters from enum header with parameters
        created from params header files
        :param params_from_enum_header_key_set: set of enum strings
        :param param_types_key_set: set of keys of param_types
        :throws RuntimeError:
        :return: None
        """
        if params_from_enum_header_key_set != param_types_key_set:
            raise RuntimeError("The parameters: {0} from param enum header "
                               "do not match the parameters names: {1} "
                               "from the config param header file. "
                               "For each enum type 'ARGUS_CONFIG_xxx' "
                               "there should be an 'xxx' parameter in "
                               "config param header file.".format(
                                   params_from_enum_header_key_set.difference(
                                       param_types_key_set),
                                   param_types_key_set.difference(
                                       params_from_enum_header_key_set)))

    def enum_to_name_and_type_dict(self):
        """
        Computes a dictionary between the enum number of a config param,
        as listed in the given enum_header_file,
        and the enum name and the type of the config param, which is declared
        in the given config_params_header_file.
        :param enum_header_file: A file containing the enum of config params.
        :param config_params_header_file: A file
            containing the type declarations of config params.
        :return: A dictionary of ints to tuples of (string, string), e.g.
        {0: ("ARGUS_CONFIG_IS_MONITOR", "INT")...}
        :raise RuntimeError: If the param names from enum_header_file do not
            match the param names in config_params_header_file.
        """
        params_from_enum_header = self.map_enum_config_types_and_values()
        param_config_map = self.map_param_config_types_to_enum()
        # check the enum and types are matched
        self._compare_params_config_types(
            set(params_from_enum_header.keys()),
            set(param_config_map.keys()))

        return {enum: (name, param_config_map[name])
                for name, enum in params_from_enum_header.items()}
