#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os

from signature_as_ext_attr import SignatureAsExtendedAttribute

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


class UnsignFS(object):
    '''
    Implements the unsign flow
    '''
    def __init__(self, path_to_unsign):
        '''
        :param path_to_unsign: path to unsign
        :type: str
        '''
        self.path_to_unsign = path_to_unsign

    def main(self):
        '''
        Unsigns path_to_unsign
        :return: None
        '''
        if os.path.isdir(self.path_to_unsign):
            return self.unset_dir()
        else:
            return self.unset_file()

    def unset_file(self):
        '''
        Unsigns a file at the provided path_to_unsign
        :return: list with file path that failed the unsign /empty
        :type: list
        :throws: Typerror if path_to_unsign is not a regular file
        '''
        signer = SignatureAsExtendedAttribute()
        if os.path.isfile(self.path_to_unsign):
            if (signer.unset_signature(self.path_to_unsign) != 0):
                return {"successes": 0, "failed_unsign": self.path_to_unsign}
            else:
                return {"successes": 1, "failed_unsign": []}
        raise TypeError(
            "Cannot unsign {}, not a regular file".format(self.path_to_unsign))

    def unset_dir(self):
        '''
        Traverses fs from provided path inwards
        to unsign every file in the fs
        :return: list of files that failed the unsign
        :type: list
        '''
        failed_unsign = []
        successes = 0
        signer = SignatureAsExtendedAttribute()
        for path, dirs, files in os.walk(self.path_to_unsign, topdown=True):
            for file_name in files:
                full_path = os.path.join(path, file_name)
                try:
                    if (signer.unset_signature(full_path) != 0):
                        failed_unsign.append(full_path)
                    else:
                        successes += 1
                except Exception as e:
                    failed_unsign.append(full_path)

        return {"successes": successes, "failed_unsign": failed_unsign}
