Table of Contents
=================
   * [General Description](#markdown-header-general-description)
   * [Prerequisites](#markdown-header-prerequisites)
   * [Signing](#markdown-header-signing)
      * [Preparations](#markdown-header-preparations)
      * [Required Arguments](#markdown-header-required-arguments)
         * [Mode](#markdown-header-mode)
         * [Flags](#markdown-header-flags)
      * [Optional Flags](#markdown-header-optional-flags)
      * [Examples of Usage](#markdown-header-examples-of-usage)
         * [Analyze Mode](#markdown-header-analyze-mode)
         * [Generate Mode](#markdown-header-generate-mode)
         * [Deploy Mode](#markdown-header-deploy-mode)
         * [Unsign Mode](#markdown-header-unsign-mode)
         * [Verify Mode](#markdown-header-verify-mode)
         * [Sign_fs Mode](#markdown-header-sign_fs-mode)
         * [Sign_fs with multiple specific files](#markdown-header-sign_fs-with-multiple-specific-files)
   * [Generating new keys for signing and verification](#markdown-header-generating-new-keys-for-signing-and-verification)
   * [Extracting public key for LSM and ASE configuration](#markdown-header-extracting-public-key-for-lsm-and-ase-configuration)


# General Description

The `sign_main.py` script is designed to cryptographically sign files across the filesystem to be deployed.
The following files are signed in the process: 

 - files with execution permission
 - files with ELF header
 - files with '#!' header
 - files listed in the `specific_files_list` *
 
The signature (encrypted hash of the file) is stored in files extended attributes.

Pay attention to use tools that support xattr (extended attributes) when handling the signed files (packing/unpacking) later on.

\* Signing specific files is an essential part of the proper signing procedure, as ASE will attempt to verify files as configured in `ase_config.yaml` at runtime.

# Prerequisites #

`sudo apt-get install attr`

Install required Python modules, run:

`pip install -r requirements.txt`

# Signing #

## Preparations ##

 - generate `specific_files_list` file based on the ASE configuration file *.
 - have rootfs directory tree ready for signing in accessible location
 - have public `key_file`** ready for signing in accessible location
 - have `certificate_file`** ready for verification in accessible location


\* `specific_files_list` should be always generated from the data in   
`argus-lsm/dynamic-config/<SYSTEM_TYPE>/ase_config.yaml` configuration file.  
To generate the list run: `argus-lsm/scripts/config/extract_list_from_yaml_conf.py`.  
During a normal signing procedure, specific_files_list should never be empty.   
If you don't want to sign any additional files, set the config to use an empty file. 

Multiple specific_files_list pathes are supported.  


\*\* `key file`:
The current key file is stored in the dynamic_config/development/hips.key in argus-lsm repository  
\*\* `certificate file`:
The current certificate file is stored in the dynamic_config/development/hips.crt in argus-lsm repository  


## Required Arguments ##

### Mode ###
`-m`, `--mode` :  
   - analyze: export hash data for signable files into an output file  
   - generate: generate signatures for files from input file,
               store signatures in the output file  
   - deploy: import and deploy signatures for files from the input file  
   - sign_fs: perform a full signing process: analyze root_fs path,  
               generate signatures for signable files and deploy signatures  
   - unsign: remove signatures from all files  
   - verify: verify deployed signatures by simulating lsm  


### Flags ##
`-p`,  `--path_to_sign`              
`-s`, `--specific_files_list_path`         
`-i`, `--input_signature_data_file_path`               
`-o`, `--output_file_data_file_path`                   
`-k`, `--key_path`                     
`-c`, `--certificate_path`  
`-t`, `--file_type` (`csv` or `json`)  

Arguments are required depending on chosen mode:

| Mode\Flag  | `-p`  | `-s`  | `-k`  | `-o`  | `-i`  | `-c`  | `-t`  |
|------------|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| analyze    | +     | +     |       | +     |       |       | +     |
| generate   |       |       | +     | +     | +     |       | +     |
| deploy     | +     |       |       |       | +     |       | +     |
| sign_fs    | +     | +     | +     |       |       |       |       |
| unsign     | +     |       |       |       |       |       |       |
| verify     | +     | +     |       |       |       | +     |       |


## Optional Flags ##
`-n`, `--no_specific_files` -  do not require path to the `specific_file_list.txt`, ignore the `specific_file_list.txt` if provided  
`-f`, `--force`  -  force sign a directory or force sign a single file                 
`--extended_signature`  -  allow signing file's content with additional data
`--include_absolute_path_in_signature`  -  sign file's path along with its content, relevant only if --extended_signature is set


## Examples of Usage ##
\* all paths are relative to current path: argus-lsm/scripts/crypto/sign_fs/  
\* names of input and output files are for the purpose of these examples  
 
 
#### Analyze mode: ####

`sudo ./sign_main.py -m analyze -p ~/path/to/sign/directory/ -n -o ~/path/to/analyzed_paths -t json`

- produces an `analyzed_paths` file that contains all signable file paths and calculated hash values  
- `-n` flag allows for not specifying the `specific_files_list_path`  


#### Generate mode: ####

`sudo ./sign_main.py -m generate -k ../../../dynamic_config/development/hips.key -i ~/path/to/analyzed_paths -o ~/path/to//signatures -t json`

- uses an `analyzed_paths` file data to generate signatures for every path in the file  
- produces a `signatures` file with signable file paths, calculated hash values and generated signatures for each path  
\* input file (analyzed_paths) must be generated by executing the tool's "Analyze" mode   
\*\* input and output file for Generate mode may be the same file  

#### Deploy mode: ####

`sudo ./sign_main.py -m deploy -i ~/path/to/signatures -t json`

- uses a `signatures` file data to deploy signatures for every path in the file  
\* input file (signatures) must be generated by executing the tool's "Generate" mode"  

#### Unsign mode: ####

`sudo ./sign_main.py -m unsign  -p ~/path/to/sign/directory/`  

- removes signatures from all files under a given path, regardless of their eligibility for signing  

#### Verify mode: ####

`sudo ./sign_main.py -m analyze -c ../../../dynamic_config/development/hips.crt -p ~/path/to/sign/directory/ -n`  

- verifies the deployed signatures using a public certificate  

### Sign_fs mode: ###
To sign the root_fs run the following command:

`sudo ./sign_main.py -m sign_fs -k ../../../dynamic_config/development/hips.key -p ~/path/to/sign/directory/ -s ~/path/to/specific_files_list`

- analyzes the file system from given path, complying with the selected arguments  
- generates signatures for all the files that passed the analysis  
- deploys the generated signatures for each file at the selected paths  
The sign_fs is the standard mode for signing, that includes all the sign stages. 

### Sign_fs with multiple specific files ###
To sign the root_fs with mutiple specific files paths run the following command:

`sudo ./sign_main.py -m sign_fs -k ../../../dynamic_config/development/hips.key -p ~/path/to/sign/directory/ -s ~/path/to/specific_files_list_1 -s ~/path/to/specific_files_list_2`  

\* any amount of specific files paths is supported, each path should be preceeded by `-s` argument  
\*\* the example shows the usage with `sign_fs` mode,  
when multiple specific files support applies to `analyze` and `verify` modes as well   


# Generating new keys for signing and verification #

- Generating private key:  
   `openssl genrsa 2048 -noout -keyout > hips.key`  
- Generating a certificate:  
   `openssl req -new -x509 -key existing_private.key -out new_certificate.crt`  

# Extracting public key for LSM and ASE configuration
Extracting public key, modulus and exponent for LSM/ASE code/configuration

 -  To extract data using private key as a source, run:
    
    `./export_public_key_for_kernel.py /path/to/hips.key -t private`
 
 -  To extract data using certificate as a source, run:
    
    `./export_public_key_for_kernel.py /path/to/hips.crt -t cert`

\* Additional info is available here
 http://stackoverflow.com/questions/41084118/crypto-akcipher-set-pub-key-in-kernel-asymmetric-crypto-always-returns-error

