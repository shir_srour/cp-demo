
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import json
from collections import OrderedDict
from file_data_io import FileDataIO


class JSONFileDataIO(FileDataIO):
    '''
    Implements reading/writing data for JSON type file_objects
    '''

    def __init__(self, file_object):
        '''
        Initializes JSONFileDataIO object
        :param file_object: opened python file object with r/w permissions
        :type: file
        '''
        self.file_object = file_object

    def read(self):
        '''
        Deserialize file_object to a python string and then
        the resulting string to a python iterable object.
        :return: list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        :throws: TypeError - when applied to a file_object of inappropriate type
        '''
        files_info = self.file_object.read()
        return json.loads(files_info)

    def write(self, files_info):
        '''
        Serialize files_info obj to a python string object.
        Then serialize the resulting string
        as a JSON formatted stream to file_object.
        :param files_info: list of dictionaries, each of form:
                    {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        :return: NoneType.
                 Writes data to the provided file_path in json format.
        :throws: TypeError - when applied to a file_object of
                    inappropriate type
        '''
        files_info_str = json.dumps(files_info, separators=(',', ': '),
                                    ensure_ascii=False, indent=4)
        self.file_object.write(files_info_str)
