#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import subprocess
import abc
import base64
from Crypto.Hash import SHA256
from collections import defaultdict, OrderedDict

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


SYMLINK = "link"
SIGNABLE = "signable"
EXTENDED_SIGNATURE_MAX_FILE_SIZE = 2 ** 32  # 4GB
SIGNING_MODE_SIZE = 1   # in bytes
SIGNATURE_LENGTH = 256  # in bytes

# ase.ko is verified by lsm, which doesn't support extended signature
EXCLUDED_FROM_EXTENDED_SIGNATURE = [
    "ase.ko", "ase.config", "ase_customer.config"]


def is_executable(file_path):
    '''Check whether or not a file is marked as executable

    :param file_path: file to check
    :return: True if the file has execution permissions, false otherwise
    '''
    return os.access(file_path, os.X_OK)


def is_script(first_4_bytes):
    '''Check whether or not a file starts with shebang

    :param first_4_bytes: first_4_bytes of a file
    :return: True if the file has a a shebang, False otherwise
    '''
    return '#!' == first_4_bytes[:2]


def is_elf(first_4_bytes):
    '''Check whether or not a file has ELF header

    :param first_4_bytes: first_4_bytes of a file
    :return: True if the file has a ELF header, False otherwise
    '''
    return '\x7fELF' == first_4_bytes


def read_first_4_bytes(file_path):
    '''Read the first 4 bytes from file_path

    :param file_path: file to read from
    :return: first 4 bytes from file_path
    '''
    with open(file_path, 'rb') as f:
        start = f.read(4)
    return start.decode("utf-8", errors="ignore")


def meets_signing_criteria(file_path):
    '''
    Returns True if the file at file path is
        either executable, shebang_script, or elf
    :return: True/False
    :type: bool
    '''
    if os.path.isfile(file_path):
        fbytes = read_first_4_bytes(file_path)
        return (is_executable(file_path) or is_script(fbytes) or is_elf(
            fbytes))
    return False


def map_file_by_criteria(relative_file_path, mapped_files):
    '''Maps file paths by pre-defined criteria
        key: SYMLINK -  file is a symbolic link
        key: SIGNABLE - file is either executable,
                                    shebang_script,
                                    or elf
    :param file_path: path to file
    :type: str
    :param mapped_files: dictionary of lists to update
                    with mapped file_paths
    :type: defaultdict
    :return: None
    '''
    if os.path.islink(relative_file_path):
        if meets_signing_criteria(relative_file_path):
            mapped_files[SYMLINK].append(relative_file_path)
        else:
            pass
    elif meets_signing_criteria(relative_file_path):
        mapped_files[SIGNABLE].append(relative_file_path)
    else:
        pass


def process_directory(path_to_analyze, mapped_files, exclude=[]):
    '''Traverses fs from provided path inwards
    to create data for mapping by criteria
    :param path_to_analyze: path to analyze fs/file
    :type: str
    :param mapped_files: dictionary of lists to map files
    :type: defaultdict
    :param exclude: list of directories to exclude from traverse
                    if none provided, defaults to empty
    :type: list
    :return: None
    '''
    cwd = os.getcwd()

    os.chdir(path_to_analyze)
    for path, dirs, files in os.walk(".", topdown=True):
        # https://stackoverflow.com/questions/19859840/excluding-directories-in-os-walk
        dirs[:] = [d for d in dirs if d not in exclude]

        for file_name in files:
            relative_file_path = os.path.normpath(
                os.path.join(path, file_name))
            if os.path.isfile(relative_file_path):
                map_file_by_criteria(relative_file_path, mapped_files)
            else:
                pass
    os.chdir(cwd)


def analyze_path(path_to_analyze, forced_run):
    '''Analyzes path according to type of path provided and input flags
    For single file as input and forced_run -
                returns path to the file without filtering
    For directory - filters out files by pre-defined criteria
    :param path_to_analyze: path to analyze fs/file
    :type: str
    :param forced_run: flag indicating forced run
    :type: bool
    :return: list of pathes filtered according to input flags
    :type: list of str
    '''

    selected_files_list = []

    if not path_to_analyze:
        return selected_files_list

    mapped_files = defaultdict(list)

    if forced_run:
        # do not filter out virtual or mounted dir
        process_directory(
            path_to_analyze, mapped_files)
        selected_files_list.extend(mapped_files[SIGNABLE])
        selected_files_list.extend(mapped_files[SYMLINK])
    else:
        # filter out virtual and mounted dirs
        excluded = set(["/dev", "/sys", "/proc", "/media"])
        process_directory(
            path_to_analyze, mapped_files, exclude=excluded)
        selected_files_list.extend(mapped_files[SIGNABLE])
    return selected_files_list


def num_to_bytes(num, length, byteorder):
    '''
    https://stackoverflow.com/a/20793663
    '''
    if byteorder != 'big' and byteorder != 'little':
        raise ValueError("byteorder must be either 'little' or 'big'")

    h = '%x' % num
    s = ('0'*(len(h) % 2) + h).zfill(length*2).decode('hex')
    return s if byteorder == 'big' else s[::-1]


class DigestProvider(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def calc_digest(self, file_path):
        '''
        Calculate digest for file at the provided path
        :param file_path: path to file to calculate digest
        :type: str
        :return: calculated digest
        :type: str
        '''
        raise NotImplementedError(
            'subclasses of DigestProvider must provide a calc_digest() method')


class HashSHA256(DigestProvider):
    '''
    Calculate digest of a file as SHA256 hash value
    '''
    HASHING_BLOCK_SIZE = 500

    def __init__(self):
        self.hashSHA256 = SHA256.new()

    @property
    def hashSHA256(self):
        return self.__hashSHA256

    @hashSHA256.setter
    def hashSHA256(self, new_digest):
        self.__hashSHA256 = new_digest

    def update_uint32(self, num):
        self.hashSHA256.update(num_to_bytes(num, length=4, byteorder='big'))

    def update_from_data(self, data, data_len_to_prepend=0):
        if data_len_to_prepend:
            self.update_uint32(data_len_to_prepend)
        self.hashSHA256.update(data)

    def update_from_file(self, file_path, file_size_to_prepend=0):
        if file_size_to_prepend:
            if file_size_to_prepend > EXTENDED_SIGNATURE_MAX_FILE_SIZE:
                raise Exception(
                    "File size must be under 4GB to be signed with --extended_signature")
            self.update_uint32(file_size_to_prepend)

        with open(file_path, 'rb') as f:
            for block in iter(lambda: f.read(self.HASHING_BLOCK_SIZE), b''):
                self.hashSHA256.update(block)

    def calc_digest(self, root_fs_path, path_in_root_fs,
                    extended_signature=False,
                    include_abs_path_in_signature=False,
                    signing_mode=""):
        '''Calculate SHA256 hash value for a file
        :param root_fs_path: the root dir of the file system to sign
        :type: str
        :param path_in_root_fs: path relative to root fs
        :type: str
        :return: calculated hash value
        :type:str
        '''
        file_path = os.path.join(root_fs_path, path_in_root_fs)

        if extended_signature:
            self.update_from_data(signing_mode, SIGNING_MODE_SIZE)

            if include_abs_path_in_signature:
                self.update_from_data(os.path.join('/', path_in_root_fs),
                                      1 + len(path_in_root_fs))

            self.update_from_file(file_path, os.path.getsize(file_path))
        else:
            self.update_from_file(file_path)

        return self.hashSHA256.hexdigest()


class AnalyzeFS(object):
    '''
    Implements the analyze data flow
    '''

    def __init__(self, root_fs_path, specific_files_list, flag_force,
                 extended_signature, include_abs_path_in_signature):
        '''
        :param root_fs_path: path to root_fs
        :type: str
        :param speific_files_list: list of specific files
                to include in analysis result
        :type: list of str
        :param flag_force: is forced run
        :type: bool
        '''
        self.root_fs_path = root_fs_path
        self.specific_files_list = specific_files_list
        self.flag_force = flag_force
        self.extended_signature = extended_signature
        self.include_abs_path_in_signature = include_abs_path_in_signature

    def is_extended_signature(self, file_path):
        if os.path.basename(file_path) in EXCLUDED_FROM_EXTENDED_SIGNATURE:
            return False
        return self.extended_signature

    def signing_mode(self, file_path):
        if self.is_extended_signature(file_path):
            signing_mode = 0
            if self.include_abs_path_in_signature:
                signing_mode |= 0b00000001
            return num_to_bytes(signing_mode, length=SIGNING_MODE_SIZE, byteorder='big')
        return ""

    def main(self):
        '''
        Analyzes file system/specific files to decide whether the
        concrete file meets the signing criteria.
        :return: list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:""}
        :type: list
        '''
        files_paths = analyze_path(self.root_fs_path, self.flag_force) + \
            self.specific_files_list

        path_hash_output = [OrderedDict(
            [("path", file_path),
             ("digest", HashSHA256().calc_digest(
                 self.root_fs_path,
                 file_path,
                 self.is_extended_signature(file_path),
                 self.include_abs_path_in_signature,
                 self.signing_mode(file_path))),
             ("signing_mode", self.signing_mode(file_path)),
             ("signature", "")]) for file_path in files_paths]

        return path_hash_output
