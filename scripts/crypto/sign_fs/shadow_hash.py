"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""


import binascii

from Crypto.Hash import SHA256

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()


class ShadowHash(SHA256.SHA256Hash, object):
    '''Create shadowing SHA256 hash object
        to enable working with Crypto.Signature.PKCS1_v1_5 module interface.
        The signer/verifier PKCS115_SigScheme.Crypto.Signature object
        requires an initialized hash object.
        However, the SHA256.SHA256Hash class
        cannot be directly initialized from
        a pre-calculated hash string.
        This class provides a workaround by
        allowing to initialize a hash object from a string.
    '''

    def __init__(self, hex_hash):
        '''Creates shadowing SHA256 hash object
        :param hex_hashed: pre calculated digest of the data in hex format
        :type: str
        '''
        super(ShadowHash, self).__init__()
        self.hex_hash = hex_hash

    def digest(self):
        '''Overloads the SHA256 hash object original digest function
        :return: the binary (non-printable) digest of the message
                    that has been hashed.
        :type: str
        '''
        return binascii.unhexlify(self.hex_hash)

    def hexdigest(self):
        '''Overloads the SHA256 hash object original hexdigest function
        :return: the printable digest of the message that has been hashed.
        :type: str
        '''
        return self.hex_hash
