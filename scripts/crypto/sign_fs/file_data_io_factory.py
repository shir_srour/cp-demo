
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

from file_data_io import FileDataIO
from json_file_data_io import JSONFileDataIO
from csv_file_data_io import CSVFileDataIO


class FileDataIOFactory(object):
    """
    Factory to create FileDataIO objects.
    Supports predefined types: CSV, JSON
    """
    FILEDATAIO_OBJECTS = {"CSV": CSVFileDataIO, "JSON": JSONFileDataIO}

    def get_file_data_io(self, file_type, file_object):
        """
        Factory method to create FileDataIO objects on demand.
        :param file_type: type of input/output file, from the supported list
        :type: str
        :param file_object: opened python file object with r/w permissions
        :return: Class to be instantiated
        :type: class
        """
        target_type = file_type.upper()
        try:
            file_io_obj = self.FILEDATAIO_OBJECTS[target_type](file_object)
        except Exception:
            raise Exception(
                """Invalid file_type supplied {}""".format(file_type))
        return file_io_obj
