#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import os
import abc
import logging

from signature_as_ext_attr import SignatureAsExtendedAttribute
from shadow_hash import ShadowHash
from analyze_paths_flow import HashSHA256
from analyze_paths_flow import EXCLUDED_FROM_EXTENDED_SIGNATURE, SIGNING_MODE_SIZE, SIGNATURE_LENGTH

from cryptography import x509
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import Encoding
from cryptography.hazmat.primitives.serialization import PublicFormat

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()

logger = logging.getLogger('sign_fs_logger')


class SignatureVerifier(object):
    '''
    Signature verifier API
    '''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def verify_signature(self, *args, **kwargs):
        """
        Verify signature abstract class
        :param args: positional arguments
        :param kwargs: named arguments
        :return: None
        """
        raise NotImplementedError(
            "subclasses of SignatureVerifier must provide a verify_signature() method")


class PKCS115_SigSchemeVerifier(SignatureVerifier):
    '''
    Verify signature with RSA public-certificate
        cryptosystem algorithm
    '''

    def __init__(self, cert_file):
        '''
        Create a signature scheme object to be used for
                    signature verification
        Create a verification scheme object to be used for
                    signature verification
        :param key_file: file opened for read with
                    public RSA certificate object
        :type: str
        '''
        self.certificate_object = x509.load_pem_x509_certificate(
            cert_file.read(), default_backend())
        self.verifier = PKCS1_v1_5.new(RSA.importKey(
            self.certificate_object.public_key().public_bytes(
                Encoding.DER, PublicFormat.PKCS1)))

    def verify_signature(self, hex_digest, signature):
        '''
        Verify signature for a given file
        :param hex_digest: pre calculated hash_value for
                verifying signature in hex format
        :type: str
        :param signature: signature str to compare with
        :type: str
        :return: True/False if calculated signature value
                matches/does not match the received signature value
        :type: boolean
        '''
        return self.verifier.verify(ShadowHash(hex_digest), signature)


class VerifySignatures(object):
    '''
    Implements the verify data flow
    '''

    def __init__(self, root_fs_path, files_info_iterable,
                 cert_file_path):
        '''
        :param file_info_iterable:  list of dictionaries, each of the form:
                {path:"/path/to/file", digest:"XXXX", signature:"YYY"}
        :type: list
        :param cert_file_path: path to file with certificate
                for validation process
        :type: str
        '''
        self.root_fs_path = root_fs_path
        self.files_info_iterable = files_info_iterable
        with open(cert_file_path, 'rb') as cert_file:
            self.verifier = PKCS115_SigSchemeVerifier(cert_file)

    def get_full_path(self, file_path):
        '''
        Creates the path full path by joining the relative input
            file_path to the root_fs_path received in init
        Normalizes the pathname by collapsing redundant separators
        :param file_path: relative path to file
        :type: str
        :return: normalized path to verify
        type: str
        '''
        full_path = os.path.join(self.root_fs_path, file_path)
        return os.path.normpath(full_path)

    def should_verify_file_with_extended_signature(self, file_path, signing_mode):
        '''
        Checks if the file should be signed with extended signature.
        :param file_path: to check
        :return: True if the flag is set
        '''
        if signing_mode == '':
            return False

        return os.path.basename(file_path) not in EXCLUDED_FROM_EXTENDED_SIGNATURE

    def is_abs_path_in_signature(self, signing_mode):
        '''
        Checks the signing_mode to see if abs_path_in_signature bit is set.
        :param signing_mode: a string containing a byte (or an empty string)
                             that encodes different options to sign a file
        :return: True if the flag is set
        '''
        if not len(signing_mode):
            return False
        return bytearray(signing_mode)[0] & 0b00000001

    def raise_if_signature_length_is_invalid(self, length,
                                             should_verify_file_with_extended_signature):
        '''
        Checks signature length (including the signing_mode if appended) is correct.
        :param length: of the signature in bytes
        :param should_verify_file_with_extended_signature: flag indicates wether
                    a signing_mode is appended to the signature
        :return: None
        '''
        expected_length = SIGNATURE_LENGTH
        if should_verify_file_with_extended_signature:
            expected_length += SIGNING_MODE_SIZE

        if length != expected_length:
            raise ValueError("Signature length must be {} "
                             "for files {} extended_signature".format(
                                 expected_length,
                                 "signed with" if should_verify_file_with_extended_signature
                                 else "excluded from"))

    def split_signature_and_mode(self, signature,
                                 should_verify_file_with_extended_signature):
        '''
        Splits the signature from the signing mode appended to it.
        :param signature: retrieved from file's xattrs
        :param should_verify_file_with_extended_signature: flag indicates wether
                    a signing_mode is appended to the signature
        :return: the signature and the signing_mode
        '''
        self.raise_if_signature_length_is_invalid(
            len(signature), should_verify_file_with_extended_signature)

        if should_verify_file_with_extended_signature:
            return signature[:-SIGNING_MODE_SIZE], signature[-SIGNING_MODE_SIZE]
        return signature, ""

    def verify_signature(self, signature, path_in_sign_fs, signing_mode):
        '''
        Verifies signature of a single file
        :param signature: retrieved from file's xattrs
        :param path_in_sign_fs: file's path relative to sign_fs
        :return: True on success False otherwise
        '''
        should_verify_file_with_extended_signature = \
            self.should_verify_file_with_extended_signature(
                path_in_sign_fs, signing_mode)

        try:
            signature, signing_mode = self.split_signature_and_mode(
                signature, should_verify_file_with_extended_signature)
        except Exception as e:
            logger.debug(
                "--Verify Fail - {}: {} --".format(e, path_in_sign_fs))
            return False

        digest = HashSHA256().calc_digest(
            self.root_fs_path, path_in_sign_fs,
            should_verify_file_with_extended_signature,
            self.is_abs_path_in_signature(signing_mode),
            signing_mode)

        return self.verifier.verify_signature(digest, signature)

    def verify(self):
        '''
        Verifies signature value for every file at the path specified
            in files_info_iterable
        Counts successes and failures
        Writes failed signature verification paths into the output list.
        :return: counted successes and a list of failed paths
        :type: dict(int, list)
        '''
        failed_verify = []
        successes = 0

        signature_reader = SignatureAsExtendedAttribute()

        for item in self.files_info_iterable:

            path_to_verify = self.get_full_path(
                item["path"])

            if not os.path.exists(path_to_verify):
                failed_verify.append(path_to_verify)
                continue
            if not os.path.isfile(path_to_verify):
                failed_verify.append(path_to_verify)
                continue
            try:
                signature = signature_reader.get_signature(
                    path_to_verify)
            except Exception:
                failed_verify.append(path_to_verify)
                continue

            if not self.verify_signature(
                    signature, item["path"], item["signing_mode"]):
                failed_verify.append(path_to_verify)
                continue

            successes += 1

        return {"successes": successes,
                "failed_paths": failed_verify}
