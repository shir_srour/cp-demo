#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved

This script converts a yaml file to binary config file for ASE.
Usage: yaml_conf_to_bin.py <YAML_FILE> <OUTPUT_BINARY_FILE>
The binary is written in big-endian order with integers written as int64 and
    consists of Type - Length - Value records.
Type is enum number identifying the specific param,
Length is the length of the value, Value - the value.
Arrays of strings are have recursive TLV
    structure.

For example, the following yaml configuration :

version: 5
ARGUS_CONFIG_IS_MONITOR: 1
ARGUS_CONFIG_ARGUS_FILES_PATHS:
    - "/sbin/auditd"
    - "/sbin/audispd"


Will look like this:

   version
| 00 00 00 05 |

  is_monitor     int length         1
| 00 00 00 00 | 00 00 00 08 | 00 00 00 00 00 00 00 01 |

  file_paths    total length of all subsequent sub TLV records
| 00 00 00 0e | 00 00 00 29 |

  arr elm type    length        /sbin/auditd
| 0f ff ff bb | 00 00 00 0c | 2f 73 62 69 6e 2f 61 75 64 69 74 64

  arr elm type    length        /sbin/audispd
| 0f ff ff bb | 00 00 00 0d | 2f 73 62 69 6e 2f 61 75 64 69 73 70 64

"""

import argparse
import os
import shutil
import yaml
import logging

from bin_config_io import BinConfig
from enum_parser import EnumParser

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object)
from future import standard_library
standard_library.install_aliases()

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
RELATIVE_CONFIG_HEADER_PATH = "../argus_config.h"
ABSOLUTE_CONFIG_HEADER_PATH = os.path.realpath(
    os.path.join(SCRIPT_DIR, RELATIVE_CONFIG_HEADER_PATH))
DYNAMIC_CONFIG_DEV_RELATIVE_PATH = "../"
DYNAMIC_CONFIG_DEV_BINARY_PATH = os.path.realpath(
    os.path.join(SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "ase.config"))
DYNAMIC_CONFIG_DEV_CONFIG_PATH = os.path.realpath(
    os.path.join(
        SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "ase_config.yaml"))


def check_yaml_config_keys(yaml_keys, enum_to_name_and_type_dict):
    """
    Checks yaml config keys against types declared in enum config
    :param yaml_keys: yaml configuration keys without version
    :param enum_to_name_and_type_dict: config types from enum
    :throws Exception: If yaml_config contains unknown values
                            (not present in enum types)
                        if the "version" field is missing
    :stdout Warning: If yaml_config does not cover all enum types,
        warning is printed with all missing types
    """
    enum_names = set([value[0]
                     for value in enum_to_name_and_type_dict.values()])
    added = yaml_keys - enum_names
    removed = enum_names - yaml_keys

    if len(removed):
        print("Warning, the following types are not covered "
              "by yaml: {}".format(', '.join(removed)))

    if len(added):
        raise ValueError(
            "Unsupported keys in yaml configuration: "
            "{}".format(', '.join(added)))


def get_header_params_data(config_header_file):
    """
    Creates a mapping between the enum number of a config param,
    as listed in the given config_header_file,
    and the enum name and the type of the config param, which is declared
    in the given config_params_header_file.
    :param config_header_file: A file containing the enum of config params.
    :param config_params_header_file: A file
        containing the type declarations of config params.
    :return: A dictionary of ints to tuples of (string, string), e.g.
    {0: ("ARGUS_CONFIG_IS_MONITOR", "INT")...}
    :raise RuntimeError: If the param names from config_header_file do not
        match the param names in config_params_header_file.
    """
    with open(config_header_file, "r") as config_params_header:
        enum_parser = EnumParser(config_params_header)
        return enum_parser.enum_to_name_and_type_dict()


def without_keys(original_dict, invalid_keys):
    """
    Creates new dict without the keys and values from
        invalid_keys list
    :param original_dict: original dictionary
    :param invalid_keys: list of invalid keys
    :return: dict
    """
    return {key: original_dict[key]
            for key in original_dict if key not in invalid_keys}


def yaml_configuration_to_bin(
        yaml_config_map, config_out_file,
        config_header_file=ABSOLUTE_CONFIG_HEADER_PATH):
    """
    Converts a YAML config to a configuration binary file.
    :param yaml_config_map: Yaml configuration dict
    :param config_out_file: binary config output file name
    :param config_header_file: path to header file with enums
    :param config_params_header_file: path to header file with
        parameters and types
    :type: str
    """
    enum_to_name_and_type_dict = get_header_params_data(
        config_header_file)

    yaml_config = without_keys(yaml_config_map, ["version"])

    check_yaml_config_keys(
        set(yaml_config.keys()), enum_to_name_and_type_dict)
    
    if not os.path.exists(os.path.dirname(config_out_file)) and \
            os.path.dirname(config_out_file) != '':
        os.makedirs(os.path.dirname(config_out_file))

    with open(config_out_file, 'wb') as bin_file:
        bin_config_io = BinConfig(bin_file)
        bin_config_io.write(yaml_config,  yaml_config_map['version'],
                            enum_to_name_and_type_dict)


def load_yaml(yaml_file):
    """
    Parse yaml file
    :param yaml_file: path to yaml file
    :return: map that represents yaml config
    """
    with open(yaml_file, 'rt') as config_file:
        try:
            yaml_config_map = yaml.safe_load(config_file)
        except yaml.YAMLError as ex:
            raise Exception(
                "Error parsing yaml configuration: # {}".format(ex))
        if 'version' not in yaml_config_map.keys():
            raise Exception(
                "Mandatory field 'version' is not present in yaml")
    return yaml_config_map


def init_logger():
    logger = logging.getLogger('y2b_logger')
    logging.basicConfig(
        level=logging.DEBUG, format='%(message)s')
    logger.propagate = False
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    logger.addHandler(ch)
    return logger


def main():
    parser = argparse.ArgumentParser(
        description="Converts a YAML config to ASE binary configuration",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-y", "--yaml_file",
                        help="YAML configuration file", type=str,
                        default=os.path.realpath(os.path.join(
                            SCRIPT_DIR, DYNAMIC_CONFIG_DEV_CONFIG_PATH)))
    parser.add_argument("-o", "--binary_out_file",
                        help="Binary config output file name", type=str,
                        default=os.path.realpath(os.path.join(
                            SCRIPT_DIR, DYNAMIC_CONFIG_DEV_BINARY_PATH)))
    parser.add_argument("--config_header_file",
                        help="Path to header file with config",
                        default=ABSOLUTE_CONFIG_HEADER_PATH, type=str)
    args = parser.parse_args()
    logger = init_logger()

    try:
        yaml_config_map = load_yaml(args.yaml_file)
        yaml_configuration_to_bin(
            yaml_config_map, args.binary_out_file,
            args.config_header_file)
    except Exception as ex:
        logger.exception(ex)
        exit(1)
    exit(0)


if __name__ == "__main__":
    exit(main())
