#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import argparse
import sys
import traceback
import os

import yaml

"""        
This script takes a diff-yaml config file path, and generate a full config yaml file, using the base yaml config file. 
The base config yaml file is the full config file with default values in it.

This script is based on specific folder/files naming/structure
The base config yaml files are located under "base" folder.
All other platfrom config files are contained in a diff-yaml file, under folders on the same location as "development" folder.
The diff-yaml file is a normal yaml file which has only the relative changes from the base yaml file, needed for this platform.
The diff-yaml files have a name pattern of '<CONF_NAME>_diff.yaml', which correspond to the base file named '<CONF_NAME>.yaml' and placed under "development" folder.

Example:
Under "base" folder you'll find atd_static.yaml which is a base config file.
Under "bsrf_soc_development" folder you'll find atd_static_diff.yaml file.
Running this script with this diff file will result with a new generated file named atd_static.yaml under "bsrf_soc_development".
"""

BASE_FOLDER_NAME = 'base'


def extract_rules(path_to_yaml):
    with open(path_to_yaml, 'r') as stream:
        configuration = yaml.safe_load(stream)
    return configuration


def main():
    parser = argparse.ArgumentParser(description="Create Yaml using a diff Yaml file")
    parser.add_argument("yaml_diff", help="ASE's yaml configuration DIFF file path", type=str)
    parser.add_argument("-o", "--config_output", type=str, default="", help="config output path")
    parser.add_argument("-b", "--base_config", type=str, default="", help="path to base config")
    args = parser.parse_args()

    diff_folder = os.path.dirname(args.yaml_diff)
    diff_file = os.path.basename(args.yaml_diff)
    generated_file_name = diff_file.replace('_diff.yaml', '.yaml')
    base_yaml_path = \
        args.base_config if args.base_config else os.path.join(diff_folder, '..', BASE_FOLDER_NAME, generated_file_name)

    # No need to change the base file
    if os.path.basename(os.path.dirname(diff_folder)) == BASE_FOLDER_NAME:
        return

    base_yaml = extract_rules(base_yaml_path)
    diff_yaml = extract_rules(args.yaml_diff)
    if diff_yaml is not None:
        for c in diff_yaml:
            base_yaml[c] = diff_yaml[c]

    output_path = args.config_output if args.config_output else os.path.join(diff_folder, generated_file_name)
    with open(output_path, 'w') as outfile:
        yaml.dump(base_yaml, outfile, default_flow_style=False)


if __name__ == "__main__":
    try:
        main()
        sys.exit(0)
    except Exception as e:
        traceback.print_exc(e)
        sys.exit(-1)
