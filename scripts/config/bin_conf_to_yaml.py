#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved

This script converts an ASE binary config file to a yaml file.
Usage: bin_conf_to_yaml.py <BINARY_FILE> <OUTPUT_YAML_FILE>
"""

import argparse
import os
import yaml
import logging
import sys

from enum_parser import EnumParser
from bin_config_io import BinConfig


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
RELATIVE_CONFIG_HEADER_PATH = "../argus_config.h"
ABSOLUTE_CONFIG_HEADER_PATH = os.path.realpath(
    os.path.join(SCRIPT_DIR, RELATIVE_CONFIG_HEADER_PATH))
DYNAMIC_CONFIG_DEV_RELATIVE_PATH = "../"
DYNAMIC_CONFIG_DEV_BINARY_PATH = os.path.realpath(
    os.path.join(SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "ase.config"))
DYNAMIC_CONFIG_DEV_CONFIG_PATH = os.path.realpath(
    os.path.join(
        SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "ase_config.yaml"))


def get_header_params_data(config_header_file):
    """
    Creates a mapping between the enum number,
    the enum name and the type of the config param, which is declared
    in the given config_header_file.
    :param config_header_file: A file
        containing the type declarations of config params.
    :return: A dictionary of ints to tuples of (string, string), e.g.
    {0: ("ARGUS_CONFIG_IS_MONITOR", "INT")...}
    :raise RuntimeError: If the param names from enum in the header file
        do not match the param names in the header file.
    """
    with open(config_header_file, "r") as config_params_header:
        enum_parser = EnumParser(config_params_header)
        return enum_parser.enum_to_name_and_type_dict()


def bin_configuration_to_yaml(
        bin_file,
        config_header_file=ABSOLUTE_CONFIG_HEADER_PATH):
    """
    Reads bytes from the given binary file, and translates them to a YAML
        that contains the names and values,
        according to the given header files.
    :param bin_file: A path to a binary file, created by yaml_conf_to_bin.py
    :param config_header_file: Path to header file with
        config param declarations
    :return: A YAML object
    """
    enum_to_name_and_type_dict = get_header_params_data(
                config_header_file)
    with open(bin_file, 'rb') as binary:
        yaml_config = BinConfig(binary).read(enum_to_name_and_type_dict)

    return yaml_config


def init_logger():
    """
    Initialize logger
    """
    logger = logging.getLogger('b2y_logger')
    logging.basicConfig(
        level=logging.DEBUG, format='%(message)s')
    logger.propagate = False
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    logger.addHandler(ch)
    return logger


def recursive_map(data, func):
    """
    Traverses the data dictionary recursively
    and applies functor to each entry
    :param data: data dictionary
    :param func: functor
    :return: data dict after applying the functor
        for each item in the dict
    """
    if isinstance(data, dict):
        accumulator = {}
        for key, value in data.items():
            accumulator[key] = recursive_map(value, func)
        return accumulator
    elif isinstance(data, (list, tuple, set)):
        accumulator = []
        for item in data:
            accumulator.append(recursive_map(item, func))
        return type(data)(accumulator)
    else:
        return func(data)


def convert_bytes_str(value):
    """
    Decodes bytes type to utf-8 string for python2
    Returns bytes type value in python 3
    :param value: value, interpreted as bytes(py3)
        or string (py2)
    :return: value decoded to string if applicable
    """
    if sys.version_info[0] < 3:
        return value
    try:
        return value.decode('utf-8')
    except (UnicodeDecodeError, AttributeError):
        return value


def main():
    parser = argparse.ArgumentParser(
        description="Converts ASE binary configuration to a YAML config")
    parser.add_argument("-b", "--binary_input_file",
                        help="Binary configuration file which "
                        "was created by 'yaml_conf_to_bin.py'", type=str,
                        default=os.path.realpath(os.path.join(
                            SCRIPT_DIR, DYNAMIC_CONFIG_DEV_BINARY_PATH)))
    parser.add_argument("-o", "--yaml_out_file",
                        help="YAML config output file name", type=str,
                        default=os.path.realpath(os.path.join(
                            SCRIPT_DIR, DYNAMIC_CONFIG_DEV_CONFIG_PATH)))
    parser.add_argument("--config_header_file",
                        help="Path to header file with "
                        "config param declarations",
                        default=ABSOLUTE_CONFIG_HEADER_PATH, type=str)

    args = parser.parse_args()
    logger = init_logger()

    try:
        yaml_config = bin_configuration_to_yaml(
            args.binary_input_file,
            args.config_header_file)

        yaml_config_mapped = recursive_map(yaml_config, convert_bytes_str)

        with open(args.yaml_out_file, 'w') as out_file:
            yaml.safe_dump(yaml_config_mapped, out_file, encoding='utf-8',
                           default_flow_style=False, allow_unicode=True)
    except Exception as ex:
        logger.error(ex)
        exit(1)
    exit(0)


if __name__ == "__main__":
    exit(main())
