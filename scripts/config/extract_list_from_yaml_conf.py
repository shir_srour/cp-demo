#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved

This script extracts a list, whose name is given as argument,
from a yaml file and writes the list as a file. Each line in the
output file fits a single element of the list.
Usage: extract_list_from_yaml_conf.py <YAML_FILE> <OUTPUT_FILE> <KEY_NAME>

The default KEY_NAME is ARGUS_CONFIG_PATHS_TO_VERIFY_SIGNATURE.
"""

import os
import re
import yaml
import struct
import argparse

from yaml_conf_to_bin import load_yaml

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
DYNAMIC_CONFIG_DEV_RELATIVE_PATH = "../../ase/dynamic_config/development/"
PATHS_TO_VERIFY_KEY = "ARGUS_CONFIG_PATHS_TO_VERIFY_SIGNATURE"


def extract_list_from_yaml_conf(yaml_file, out_file, key_name=PATHS_TO_VERIFY_KEY):
    """
    Extracts a list, whose name is `key_name` from the given YAML file and writes the list to
    the out_file. Each line in the output file fits a single element of the list.
    :param yaml_file: A YAML configuration file.
    :param out_file: Output file path.
    :param key_name: The name of the key to extract.
    """
    print "Generating '{}', using key '{}', from '{}'\n".format(out_file, key_name, yaml_file)
    yaml_conf = load_yaml(yaml_file)

    if key_name not in yaml_conf:
        raise Exception("Required field '{}' is not present in yaml".format(key_name))

    list_value = yaml_conf[key_name]
    if type(list_value) is not list:
        raise Exception("Required field '{}' is not a list".format(key_name))

    # Add new lines
    list_value = [list_element + os.linesep for list_element in list_value]

    with open(out_file, 'wb') as out_fd:
            out_fd.writelines(list_value)
    
    print "File '{}' created successfully".format(out_file)

def main():
    parser = argparse.ArgumentParser(description="Extracts a list from a YAML config. For "
                                                 "example, the list of "
                                                 "ARGUS_CONFIG_PATHS_TO_VERIFY_SIGNATURE can "
                                                 "be used as input to sign_fs.py",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-y", "--yaml_file", help="YAML configuration file", type=str,
                        default=os.path.realpath(os.path.join(SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "ase_config.yaml")))
    parser.add_argument("-o", "--out_file", help="Output file name", type=str,
                        default=os.path.realpath(os.path.join(SCRIPT_DIR, DYNAMIC_CONFIG_DEV_RELATIVE_PATH + "specific_files_list.txt")))
    parser.add_argument("--key_name", help="The name of list key name"
                                           " in the YAML config.",
                        default=PATHS_TO_VERIFY_KEY, type=str)
    args = parser.parse_args()

    extract_list_from_yaml_conf(**vars(args))


if __name__ == "__main__":
    main()
