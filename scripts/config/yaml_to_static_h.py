#!/usr/bin/env python3
"""
(C) Copyright Argus Cyber Security Ltd
All rights reserved
"""

import argparse
import collections
import sys
import time
import traceback
import os

import yaml

"""        
This script iterates over Yaml config files and generates a H static config file.

This script is based on specific folder/files naming/structure
All config key names in the Yaml should start with the prefix 'ARGUS_CONFIG_'

"""

YAML_PREFIX = "ARGUS_CONFIG_"
H_PREFIX = "CONFIG_DEFAULT_"


def isprintable(s, codec='utf8'):
    """
    Check if this string contains ascii printable chars, or not.
    This is used to distinguish between text strings to binary buffers
    """
    try:
        s.decode(codec)
        return next((x for x in s if ord(x) < 32 or ord(x) > 126), None) == None
    except UnicodeDecodeError:
        return False
    else:
        return True


def to_hex_list(s):
    """
    Make a chars list from a string
    """
    return map(hex, map(ord, s))


def extract_conf_data_from_yaml(path_to_yaml):
    with open(path_to_yaml, 'r') as stream:
        configuration = yaml.safe_load(stream)
    return configuration


def convert_conf_data_to_h(conf_data):

    h_content = ""
    if 'version' in conf_data:
        conf_data.pop('version')
    for r in sorted(conf_data):
        try:
            new_name = r.replace(YAML_PREFIX, H_PREFIX)
            if type(conf_data[r]) == bool:
                h_content += "#define {} {}\n".format(new_name,
                                                      str(conf_data[r]).lower())
            elif type(conf_data[r]) == int:
                h_content += "#define {} {}\n".format(new_name,
                                                      str(conf_data[r]))
            elif type(conf_data[r]) == str:
                if isprintable(conf_data[r]):
                    apostrophies = '"'
                    if (conf_data[r][0].isdigit()) and (conf_data[r][-1] == 'U'):
                        apostrophies = ''
                    h_content += '#define {0} {2}{1}{2}\n'.format(new_name,
                                                                  conf_data[r], apostrophies)
                else:
                    h_content += "#define {} {{".format(new_name)
                    h_content += ", ".join(map(str, to_hex_list(conf_data[r])))
                    h_content += "}\n"
            elif type(conf_data[r]) == list:
                h_content += "#define {} {{".format(new_name)
                if len(conf_data[r]) > 0:
                    if isprintable(conf_data[r][0]):
                        apostrophies = '"'
                        if conf_data[r][0].startswith('&'):
                            apostrophies = ''
                        h_content += apostrophies + \
                            (apostrophies + ', ' +
                             apostrophies).join(map(str, conf_data[r])) + apostrophies
                    else:
                        for element in conf_data[r]:
                            h_content += ", ".join(map(str,
                                                       to_hex_list(element)))
                            h_content += ", "
                        h_content = h_content[:-2]
                h_content += "}\n"
            else:
                print ("Skipping rule: " + r)
        except:
            print ("Failed parsing rule named: " + r)
            raise
    return h_content


def get_h_content(config_dir):
    h_content = ""
    for filename in os.listdir(config_dir):
        if filename.endswith(".yaml") and not filename.endswith("diff.yaml"):
            conf_data = extract_conf_data_from_yaml(
                os.path.join(config_dir, filename))
            h_content += convert_conf_data_to_h(conf_data)
    return h_content


def main():
    parser = argparse.ArgumentParser(
        description="Create H static config file using a Yaml config file")
    parser.add_argument("-d", "--dynamic_dir",
                        help="ASE's dynamic config platfrom path", type=str, required=True)
    parser.add_argument("-s", "--static_dir",
                        help="ASE's static config platfrom path", type=str, required=True)
    parser.add_argument("-t", "--template",
                        help="template file for the generate H file", type=str, required=True)
    args = parser.parse_args()

    h_content = get_h_content(args.dynamic_dir)
    h_content += get_h_content(args.static_dir)

    static_conf_folder = os.path.dirname(args.template)
    generated_h_file_name = "ase_static_config.h"

    h_template = ""
    with open(args.template, 'r') as stream:
        h_template = stream.read()

    h_template = h_template.replace("DUMMY_STRING", h_content)

    with open(static_conf_folder + '/' + generated_h_file_name, 'w') as outfile:
        outfile.write(h_template)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        traceback.print_exc(e)
        sys.exit(-1)
