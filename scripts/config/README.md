# README #

### How to produce ASE configuration binary file from text (yaml)  ###
Files in these directory:
========================

1. ase_config.yaml - yaml parameters file.
2. yaml_conf_to_bin.py - script which converts a YAML config to a binary parameters file(blob).
3. bin_conf_to_yaml.py - script which converts a binary parameters file(blob) to a YAML config.

## How to use yaml_conf_to_bin.py ##

1. Add new configuration parameres (if any) to ase_config.yaml
2. Run yaml_conf_to_bin.py with updated yaml file as parameter
    script yaml_conf_to_bin.py will produce binary configuration file binary_config

    ```
    python yaml_conf_to_bin.py -y ase_config.yaml -o binary_config
    ```

3. Sign output file
    
    ```
    sudo ~/argus-lsm/scripts/crypto/sign_fs.py binary_config -f 
    ```

    \* for more details on the signing process, see `argus-lsm/scripts/crypto/README.md`

4. Copy signed file to /etc/argus/ase.config

    ```
    sudo mv binary_config /etc/argus/ase.config
    ```

## How to use bin_conf_to_yaml.py ##
1. Copy the config from the target dir

    ```
    sudo cp /etc/argus/ase.config binary_config 
    ```

2. Run bin_conf_to_yaml.py with the binary configuration as parameter
    bin_conf_to_yaml.py will produce a YAML configuration file from binary_config

    ```
    bin_conf_to_yaml.py binary_config yaml_config.yaml
    ```