/**
 * @file
 *
 * (C) Copyright Argus Cyber Security Ltd
 * All rights reserved
 *
 * This file defines config param types and various fields length in the binary format.
 * !! Do not change the the values of the enum!
 * It is used both by LSM and by offline framework.
 * !! Do not add anything else to this file and do not change the enum format,
 * it is parsed by offline framework which looks for strings with ARGUS_CONFIG_ prefix.
 *
 * Each enum value is sequential and fixed.
 * The idea is to maintain:
 *   - the fixed values, so the enum is easily parsable in binary form,
 *   - the sequential values, because our infrastructure requires that.
 * Do not change the values of existing parameters!
 */
#ifndef ARGUS_CUSTOMER_CONFIG_PARAMS_TYPES_H
#define ARGUS_CUSTOMER_CONFIG_PARAMS_TYPES_H


#include "include/config_framework.h"


/**
 * Values that represent argus Configuration Parameter names.
 * !! This enum is used by customers and by the tuning process.
 * !! Do not change the enum!
 * Each param name have to start with ARGUS_CONFIG prefix so that offline framework can parse it.
 * Note that param names must match the variable names listed in this file, in the following way:
 * Given a variable name v, whose matching param name is p, than "ARGUS_CONFIG_" + uppercase(v) = p.
 * For example, for a param name ARGUS_CONFIG_VERIFY_ON_USAGE_PATHS, the matching variable name must be verify_on_usage_paths.
 */
enum argus_customer_config_param_name
{
	ARGUS_CONFIG_VERIFY_ON_USAGE_PATHS = 0,
	ARGUS_CONFIG_REVOKED_SIGNATURES = 1,
	ARGUS_CONFIG_PUBLIC_KEY = 2,
	ARGUS_CONFIG_PUBLIC_KEY_MODULUS = 3,
	ARGUS_CONFIG_PUBLIC_KEY_EXPONENT = 4,
	ARGUS_CONFIG_CUSTOMER_MAX_TYPE /**< This should be the last enum */
};


/**
 * Argus configuration params, define global param variables that can be used by all other logic.
 * Note that variable names must match the param names listed in enum argus_config_param_name,
 * in the following way:
 * Given a variable name v, whose matching param name is p, than "ARGUS_CONFIG_" + uppercase(v) = p.
 * For example, for a variable name is_monitor, the matching param name must be ARGUS_CONFIG_IS_MONITOR.
 *
 * @see argus_config_framework.h
 * @see https://drive.google.com/open?id=1LmsS2VjNiQV7SBA_9ZCoAjsiwvIrNp6aUhQ7u6xigYI
 */

/**
 * Description: List of customer files to verify signature on usage
 * When a process attempts to open a file in ARGUS_CONFIG_VERIFY_ON_USAGE_PATHS list for reading, if the file signature verification fails ASE shall write a log message and block the open action if ARGUS_CONFIG_SHOULD_PREVENT_UNVERIFIED_SENSITIVE_FILE_USAGE set to true.
 * Valid values: Array of strings. Each string is a file to check signature. Maximum path size is defined by INTERNAL_CONFIG_MAX_PATH_SIZE. Maximum number of files in the array is defined by INTERNAL_CONFIG_ON_USAGE_VERIFICATION_MAX_PATHS.
 * A path is valid if the string starts with '/' since we're not dealing with relative paths.
 * Dependencies:None.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN_INCLUDING_DEFAULT(verify_on_usage_paths)

/**
 * Description: list of first 8 bytes of module's or any signed file's signature to prevent rollback attack.
 * when verifying file signature is not if one of the binary buffers in 'ARGUS_CONFIG_REVOKED_SIGNATURES' configuration key is a prefix of the buffer stored in the file signature attributes the CP writes a log message and prevents the execution.
 * Valid values: Maximum number of revoked signatures is defined by ARGUS_CONFIG_MAX_REVOKED_SIGNATURES. signed object has signature string of 512 bytes length.
 * Dependencies : ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE , ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITHOUT_SIGNATURE.
 */
ARGUS_PARAM_ARR_BIN_DECLARE_EXTERN(revoked_signatures)

/**
 * Description: Public key bytes
 * When verify a file signature, the signature is read out of the file's extended attributes and RSA PKCSv1.5 signature is verified using kernel crypto API.
 * RSA key composed of the configuration entries 'ARGUS_CONFIG_PUBLIC_KEY', 'ARGUS_CONFIG_PUBLIC_KEY_MODULUS' and 'ARGUS_CONFIG_PUBLIC_KEY_EXPONENT'.
 * Valid values: Maximum public key length is defined by INTERNAL_CONFIG_PUBLIC_KEY_MAX_LENGTH.
 * Dependencies: ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE.
 */
ARGUS_PARAM_BIN_DECLARE_EXTERN(public_key)

/**
 * Description: Public key modules
 * When verify a file signature, the signature is read out of the file's extended attributes and RSA PKCSv1.5 signature is verified using kernel crypto API.
 * RSA key composed of the configuration entries 'ARGUS_CONFIG_PUBLIC_KEY', 'ARGUS_CONFIG_PUBLIC_KEY_MODULUS' and 'ARGUS_CONFIG_PUBLIC_KEY_EXPONENT'.
 * Valid values: Maximum public key modulus length is defined by INTERNAL_CONFIG_PUBLIC_KEY_MODULUS_LENGTH.
 * Dependencies: ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE.
 */
ARGUS_PARAM_BIN_DECLARE_EXTERN(public_key_modulus)

/**
 * Description: Public key exponent
 * When verify a file signature, the signature is read out of the file's extended attributes and RSA PKCSv1.5 signature is verified using kernel crypto API.
 * RSA key composed of the configuration entries 'ARGUS_CONFIG_PUBLIC_KEY', 'ARGUS_CONFIG_PUBLIC_KEY_MODULUS' and 'ARGUS_CONFIG_PUBLIC_KEY_EXPONENT'.
 * Valid values: Maximum public key exponent length is defined by INTERNAL_CONFIG_PUBLIC_KEY_EXPONENT_LENGTH.
 * Dependencies: ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE.
 */
ARGUS_PARAM_BIN_DECLARE_EXTERN(public_key_exponent)

/**
 * @Global_doc
 * Name: customer_config_param_functions_table.
 * Type: Array of struct config_param_entry.
 * Description: A global table mapping argus_customer_config_param_name enums and function pointers for parsing values and setting values to default.
 */
extern struct config_param_entry
	customer_config_param_functions_table[ARGUS_CONFIG_CUSTOMER_MAX_TYPE];

/** Initializes the customer configuration parameter functions table */
void init_customer_config_param_functions_table(void);


#endif /** ARGUS_CUSTOMER_CONFIG_PARAMS_TYPES_H */

