/**
 * @file
 *
 * (C) Copyright Argus Cyber Security Ltd
 * All rights reserved
 *
 * This file defines config param types and various fields length in the binary format.
 * !! Do not change the the values of the enum!
 * It is used both by LSM and by offline framework.
 * !! Do not add anything else to this file and do not change the enum format,
 * it is parsed by offline framework which looks for strings with ARGUS_CONFIG_ prefix.
 *
 * Each enum value is sequential and fixed.
 * The idea is to maintain:
 *   - the fixed values, so the enum is easily parsable in binary form,
 *   - the sequential values, because our infrastructure requires that.
 * There is a UT that checks the match between existing enum values.
 * !!! Parameter with enum value of 40 is used by the client!
 * Do not change the values of existing parameters!
 */
#ifndef ARGUS_CONFIG_PARAMS_TYPES_H
#define ARGUS_CONFIG_PARAMS_TYPES_H


#include "include/config_framework.h"


/**
 * Values that represent argus Configuration Parameter names.
 * !! This enum is used by customers and by the tuning process.
 * !! Do not change the enum!
 * Each param name have to start with ARGUS_CONFIG prefix so that offline framework can parse it.
 * Note that param names must match the variable names listed in this file, in the following way:
 * Given a variable name v, whose matching param name is p, than "ARGUS_CONFIG_" + uppercase(v) = p.
 * For example, for a param name ARGUS_CONFIG_IS_MONITOR, the matching variable name must be is_monitor.
 */
enum argus_config_param_name
{
	ARGUS_CONFIG_IS_MONITOR = 0,
	ARGUS_CONFIG_MONITOR_MMAP = 1,

	ARGUS_CONFIG_FILE_TYPE_STANDARD_STDIN_DEVICE = 2,
	ARGUS_CONFIG_FILE_TYPE_PIPING_STDIN_DEVICE = 3,
	ARGUS_CONFIG_FILE_TYPE_REGULAR_FILE = 4,

	ARGUS_CONFIG_SECURITY_DOMAIN = 5,
	ARGUS_CONFIG_SCRIPT_FILE_HANDLE = 6,

	ARGUS_CONFIG_ARGUS_FILES_PATHS = 7,
	ARGUS_CONFIG_SHELL_C_ALLOWED_PATHS = 8,
	ARGUS_CONFIG_SHELL_PIPING_ALLOWED_PATHS = 9,

	ARGUS_CONFIG_PATHS_TO_VERIFY_SIGNATURE = 10,

	ARGUS_CONFIG_BLOCK_MODULE_LOAD_REVOKED_SIGNATURE = 11,
	ARGUS_CONFIG_BLOCK_MODULE_LOAD_SIG_ERROR = 12,
	ARGUS_CONFIG_BLOCK_MODULE_LOAD_SIG_NOT_MATCH = 13,
	ARGUS_CONFIG_BLOCK_MODULE_LOAD_NO_SIGNATURE = 14,
	ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED = 15,

	ARGUS_CONFIG_KERN_BLOCK_MODULE_LOAD_FROM_BUFFER = 16,
	ARGUS_CONFIG_KERNEL_PARAMS_WHITELIST = 17,
	ARGUS_CONFIG_PROTECT_KERNEL_PARAMETERS_ENABLED = 18,

	ARGUS_CONFIG_SHOULD_BLOCK_MOUNT = 19,
	ARGUS_CONFIG_SHOULD_BLOCK_REMOUNT = 20,
	ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITHOUT_SIGNATURE = 21,
	ARGUS_CONFIG_SHOULD_BLOCK_PTRACE = 22,
	ARGUS_CONFIG_SHOULD_BLOCK_KERNEL_PARAMS = 23,
	ARGUS_CONFIG_SHOULD_BLOCK_XATTR = 24,
	ARGUS_CONFIG_SHOULD_PREVENT_UNVERIFIED_SENSITIVE_FILE_USAGE = 25,
	ARGUS_CONFIG_SHOULD_PREVENT_FILES_ACCESS = 26,
	ARGUS_CONFIG_SHOULD_PREVENT_SIGNAL = 27,
	ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITH_INVALID_SIGNATURE = 28,
	ARGUS_CONFIG_SHOULD_PREVENT_REMOUNT = 29,
	ARGUS_CONFIG_SHOULD_PREVENT_PTRACE = 30,
	ARGUS_CONFIG_SHOULD_PREVENT_MODIFY_SECURITY_XATTR = 31,
	ARGUS_CONFIG_SHOULD_PREVENT_MOUNT = 32,
	ARGUS_CONFIG_SHOULD_PREVENT_DIRTY_EXEC = 33,
	ARGUS_CONFIG_SHOULD_PROTECT_SIGNED_FILES = 34,
	ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE = 35,
	ARGUS_CONFIG_SHOULD_USE_VERIFICATION_CACHE = 36,
	ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE_ON_SYMLINKS = 37,
	ARGUS_CONFIG_ENABLE_SIGNING_MODES = 38,
	ARGUS_CONFIG_SHOULD_BLOCK_INTERACTIVE_SHELL = 39,
	ARGUS_CONFIG_SHOULD_PREVENT_FOLLOWING_SYMLINKS_WITHOUT_VERIFIED_SIGNATURE = 40,


	ARGUS_CONFIG_MAX_TYPE /**< This should be the last enum */
};


/**
 * Argus configuration params, define global param variables that can be used by all other logic.
 * Note that varialbe names must match the param names listed in enum argus_config_param_name,
 * in the following way:
 * Given a variable name v, whose matching param name is p, than "ARGUS_CONFIG_" + uppercase(v) = p.
 * For example, for a variable name is_monitor, the matching param name must be ARGUS_CONFIG_IS_MONITOR.
 *
 * @see argus_config_framework.h
 * @see https://drive.google.com/open?id=1LmsS2VjNiQV7SBA_9ZCoAjsiwvIrNp6aUhQ7u6xigYI
 */

/**
 * Validates multiple paths described by value
 *
 * @param value The value.
 *
 * @return true if path is valid, false otherwise
 */
argus_bool validate_paths(const struct value_type *value);

/**
 * Description: If set to true, LSM shall send events that are managed in ATD: mmap, execute, write, exit (process done running), net ip set and net mast set.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(is_monitor)

/**
 * Description: Monitor and enable blocking of mount operation
 * If set to true, when a process attempts to mount a device the ASE will send a log message of category 'ForbiddenMount' and triggers the action.
 * Valid values: True/False
 * Dependencies: None
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_mount)

/**
 * Description: Monitor and enable blocking of remount operation
 * If set to true, when a process attempts to trace another process the ASE will send a log message of category 'ForbiddenMount' and triggers the action.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_remount)

/**
 * Description: Block attempt to change kernel parameters
 * If set to true, block a process that attempts to change kernel parameters and its file is not in ARGUS_CONFIG_KERNEL_PARAMS_WHITELIST.
 * The ASE send a log message of level 'block' and block this action.
 * Valid values: True/False
 * Dependencies: ARGUS_CONFIG_PROTECT_KERNEL_PARAMETERS_ENABLED.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_kernel_params)

/**
 * Description: Monitor Log MMap
 * If set to true, LSM will send mmap events for the ATD
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(monitor_mmap)

/**
 * Description: Verify signatures
 * If set to true ASE will verify signature
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_verify_signature)

/**
 * Description: if set to true, a file with no signature will be considered as one with invalid signatures.
 * If set to true and file has no signature, ASE will treat the file as as defined by ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITH_INVALID_SIGNATURE.
 * If set to false and file has no signature, ASE will write a log message of category 'VerifySignature',
 * but the action will be allowed and not blocked.
 * See also: 'ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE' and 'ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITH_INVALID_SIGNATURE'.
 * Valid values:True\False
 * Dependencies: none.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_files_without_signature)

/**
 * Description: Alert ptrace attempts
 * If set to true, when a process that is not on the 'ARGUS_CONFIG_ARGUS_FILES_PATHS' list attempts to trace another process the CP shall writes a log message
 * of category 'MemoryAttach'.  if 'ARGUS_CONFIG_SHOULD_PREVENT_PTRACE'  is set to 1,  triggers the action and the trace attempt is prevented.
 * Valid values: 0/1
 * Dependencies:ARGUS_CONFIG_ARGUS_FILES_PATHS
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_ptrace)

/**
 * Description: Monitor and enable blocking of xattrs get/set attempts
 * If set to true, when a process attempts to remove security extended attributes for a file the ASE will send a log message.
 * Valid values:True\False
 * Dependencies:None
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_xattr)

/**
 * Description: Block and monitor attempt to change kernel parameters
 * If set to false, allows a process to change kernel parameters.
 * If set to true, Ase shall monitor kernel parameter modification by processes that are not allowed to. See  ARGUS_CONFIG_KERNEL_PARAMS_WHITELIST.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(protect_kernel_parameters_enabled)

/**
 * Description: Block on attempt to open unverified on-usage-verification file
 * If set to true,
 * If set to true, when a process attempts to open a file in ARGUS_CONFIG_VERIFY_ON_USAGE_PATHS list for reading, if the file signature verification fails ASE shall write a log message and block the open action.
 * Valid values:True\False
 * Dependencies: 'ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE'
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_unverified_sensitive_file_usage)

/** If set, argus_lsm will block on attempt to execute an interactive shell process (always logs). */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_block_interactive_shell)

/**
 * Description: The handle of a script file
 * Interested only in shells opening files with the specific handle given in the configuration which is typical handle number for scripts.
 * Valid values: OS-specific value for identifying file handler.
 * Dependencies:None.
 */
ARGUS_PARAM_INT_DECLARE_EXTERN(script_file_handle)

/**
 * Description: Standard stdin mode
 * Checks whether the process' stdin is a standard stdin device.
 * Valid values: OS-specific value for identifying standart stdin device
 * Dependencies: None.
 */
ARGUS_PARAM_INT_DECLARE_EXTERN(file_type_standard_stdin_device)

/**
 * Description: Stdin mode to allow by piping whitelist.
 * check if a process that run a shell command use a forbidden stdin or the stdin mode is in the piping whitelist.
 * Valid values: OS-specific value for identifying stdin device
 * Dependencies: None.
 */
ARGUS_PARAM_INT_DECLARE_EXTERN(file_type_piping_stdin_device)

/**
 * Description: Ase receive this GPL value from configuration to identify regular file to avoid infringing copyrights.
 * Valid values: OS-specific value for identifying regular files
 * Dependencies: None.
 */
ARGUS_PARAM_INT_DECLARE_EXTERN(file_type_regular_file)

/**
 * Description: Monitor and block attempts to access argus files by non argus process.
 * If set to true, if a process is not an argus process try to access (open, rename, truncate, cmod, chown and remove) a file in 'ARGUS_CONFIG_ARGUS_FILES_PATHS' list the ASE shall write a log message  and prevent the attempt.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_files_access)

/**
 * Description: Prevent any attempt to execute code from an unverified file
 * If set to true and  if
 * a user space executable is executed or a process maps a file to memory with execution permission
 * and 'ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE' is true,
 * and none of the binary buffers in the value of the 'ARGUS_CONFIG_REVOKED_SIGNATURES' configuration key is a substring of the buffer stored in the file attributes
 * and the buffer stored in the attribute is not equal to the RSA PKCSv1.5 signature of the file content's Sha256 output,
 *
 * the CP shall write a log message of category 'VerifySignature', trigger the action and block the execution from happening.
 *
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_SHOULD_VERIFY_SIGNATURE and ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITHOUT_SIGNATURE
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_files_with_invalid_signature)

/**
 * Description: Allows/blocks signals to an Argus process
 * If set to true, if a process sends a kill signal and the sending and receiving process's path do not appear on the 'ARGUS_FILES_PATHS' configuration list
 * and the sending process is not the parent process of the receiving process, the CP shall write a log message of category 'AccessArgusFiles',
 * triggers the action and prevent the attempt.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_signal)

/**
 * Description: allow/block remount operation
 * If set to true, when a process attempts to trace another process the CP writes a log message and block remount action.
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_SHOULD_BLOCK_REMOUNT
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_remount)

/**
 * Description: Block ptrace attempts
 * If set to true, if a process that is not on the 'ARGUS_CONFIG_ARGUS_FILES_PATHS' list attempts to trace another process,
 * the CP shall write a log message and the trace attempt is prevented.
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_SHOULD_BLOCK_PTRACE
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_ptrace)

/**
 * Description: Block xattrs get/set attempts
 * If set to true, when a process attempts to remove security extended attributes for a file the CP writes a log message and prevent the attempt.
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_SHOULD_BLOCK_XATTR
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_modify_security_xattr)

/**
 * Description: Allow/block mount  operation
 * If set to true, when a process attempts to trace another process the CP writes a log message and block mount action.
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_SHOULD_BLOCK_MOUNT
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_mount)

/**
 * Description: Prevent dirty execution
 * If set to true, prevent execution of processes where the environment contains suspicious environment variables that are forbidden.
 * the CP shall write a log message and block the execution.
 * Prevents execution if the process is a shell command that uses a forbidden stdin.
 * Prevents execution if the process run a shell c and its path is not listed under 'ARGUS_CONFIG_SHELL_C_ALLOWED_PATHS' configuration entry.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_dirty_exec)

/**
 * Description: Signature domain is a string that is added to the security attributes as a part of the signature process.
 * If any process tries to delete the signature from the security attributes and 'ARGUS_CONFIG_SHOULD_BLOCK_XATTR' configuration is set to true,
 *  ASE shall send a log message to ATD
 * @Possible values: Maximum length of security domain is defined by INTERNAL_CONFIG_MAX_SECURITY_DOMAIN in internal_config.
 * Dependencies:ARGUS_CONFIG_SHOULD_BLOCK_XATTR
 */
ARGUS_PARAM_STR_DECLARE_EXTERN(security_domain)

/**
 * Description: List of Argus files to be protected from writing or plain reading without execution.
 * Valid values: Array of strings. Each string is a file to check signature. Maximum path size is defined by INTERNAL_CONFIG_MAX_PATH_SIZE.
 * Maximum number of files in the array is defined by INTERNAL_CONFIG_MAX_PATHS.
 * A path is valid if the string starts with '/' since we're not dealing with relative paths.
 * Dependencies: None.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN(argus_files_paths)

/**
 * Description: List of files to check signature when the Logic Engine starts. Verification is called by theATD on the configuration files.
 * Valid values: Array of strings. Each string is a file to check signature.
 * Maximum path size is defined by INTERNAL_CONFIG_MAX_PATH_SIZE. Maximum number of files in the array is defined by INTERNAL_CONFIG_MAX_PATHS.
 * A path is valid if the string starts with '/' since we're not dealing with relative paths.
 * Dependencies: INTERNAL_CONFIG_MAX_PATH_SIZE and INTERNAL_CONFIG_MAX_PATHS.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN(paths_to_verify_signature)

/**
 * Description: List of binaries allowed to execute shell -c
 * When process invokes a shell executable and process' path is not listed under 'ARGUS_CONFIG_SHELL_C_ALLOWED_PATHS' configuration entry, the ASE shall writes a log message.
 * Valid values: Array of strings. Each string is a file to check signature. Maximum path size is defined by INTERNAL_CONFIG_MAX_PATH_SIZE. Maximum number of files in the array is defined by INTERNAL_CONFIG_MAX_PATHS.
 * A path is valid if the string starts with '/' since we're not dealing with relative paths.
 * Dependencies: None.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN(shell_c_allowed_paths)

/**
 * Description: List of binaries allowed to pipe shell command
 * Use: When a process invokes a shell executable or pipes stdin into the shell and the
 * path of the process is not listed under
 * 'ARGUS_CONFIG_SHELL_PIPING_ALLOWED_PATHS'  the ASE shall write a log message.
 * Valid values: Array of strings. Each string is a file to check signature.
 * Maximum path size is defined by INTERNAL_CONFIG_MAX_PATH_SIZE. Maximum number of files in the array is defined by INTERNAL_CONFIG_MAX_PATHS.
 * Dependencies: None.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN(shell_piping_allowed_paths)

/**
 * Description: Module load protection -
 * If set to true and the requested module owns a revoked signature, Ase shall send a log message with level 'block' and block the loading of the module.
 * If set to false and the requested module owns a revoked signature, Ase shall send a log message with level 'log' and do not prevent the loading of the module.
 * Valid values:True\False
 * Dependencies:ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(block_module_load_revoked_signature)

/**
 * Description: Module load protection -
 * If set to true and the signature calculation outcome is an error,  Ase shall send a log message with level 'block' and block the loading of the module.
 * If set to false and the signature calculation outcome is an error,  Ase shall send a log message with level 'log' and do not prevent the loading of the module.
 * Valid values:True\False
 * Dependencies:ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(block_module_load_sig_error)

/**
 * Description: Module load protection -
 * If set to true and the module's signature does not match any known signature, Ase shall send a log message with level 'block' and prevent loading of the module.
 * If set to false and the module's signature does not match any known signature, Ase shall send a log message with level 'log' and do not prevent loading of the module.
 * Valid values: True\False
 * Dependencies: ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(block_module_load_sig_not_match)

/**
 * Description: Module load protection -
 * If set to true and the requested module doesn't own a signature, Ase shall send a log message with level 'block' and block the loading of the module.
 * If set to false and the requested module doesn't own a signature, Ase shall send a log message with level 'log' and do not prevent the loading of the module.
 * Valid values:True\False
 * Dependencies: ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED must be set for this configuration to take actions.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(block_module_load_no_signature)

/**
 * Description: If set to true, enables feature of module load protection.
 * Ase shall monitor modules loading requests based on the module data and signature.
 * Valid values: True\False
 * Dependencies:None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(block_module_load_enabled)

/**
 * Description: Protect files with signatures
 * If set to true:
 * 1. When verifying a signature for the first time and the file is not opened for write, ASE will make the file read only (immutable)
 * 2. when process attempts to remove the immutable attribute from a signed file, the CP will write a log message and prevent the attempt.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_protect_signed_files)

/**
 * Description: Enables prevention of module load using buffer from user space
 * If set to true and attempt to load model from buffer,  Ase shall send a log message with level 'block' and block the loading of the module.
 * If set to false and load model from buffer,  Ase shall send a log message with level 'log' and do not prevent the loading of the module.
 * Valid values:True\False
 * Dependencies:ARGUS_CONFIG_BLOCK_MODULE_LOAD_ENABLED.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(kern_block_module_load_from_buffer)

/**
 * Description: Store and use signatures cache
 * If set to true, when verifying signature on file, if file id does not exist in the cache lookup, verify file signature and add the file id and signature to the cache.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_use_verification_cache)

/**
 * Description: Verify signatures of all symlinks and not only files. The verification will be performed on every symlink follow.
 * Not implemented yet.
 * Valid values:True\False
 * Dependencies: None.
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_verify_signature_on_symlinks)

/** If set, argus_lsm will prevent symlinks from being followed unless they have a verified signature */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(should_prevent_following_symlinks_without_verified_signature)

/** If set, argus_lsm will verify signature according to its signing mode. */
/**
 * Description: Verify signature according to its signing mode
 * If set, check the signing mode appended to the signature (as a hint to the verification process) and perform the hash calculation according to its value.
 * If not set, do not check the signing_mode appended to the signature but assume it is '\x00' and perform the hash calculation based on that assumption.
 * Valid values:True\False
 * Dependencies: None
 */
ARGUS_PARAM_BOOL_DECLARE_EXTERN(enable_signing_modes)

/**
 * Description: List of scripts/binaries allowed to change kernel parameters
 * CP can alert/block a process that attempts to modify kernel parameters that its file does not in ARGUS_CONFIG_KERNEL_PARAMS_WHITELIST list.
 * Valid values: Maximum path size and number of files in various arrays are defined by INTERNAL_CONFIG_MAX_PATHS and INTERNAL_CONFIG_MAX_PATH_SIZE.
 * Dependencies: INTERNAL_CONFIG_MAX_PATHS and INTERNAL_CONFIG_MAX_PATH_SIZE.
 */
ARGUS_PARAM_ARR_STR_DECLARE_EXTERN(kernel_params_whitelist)

/**
 * @Global_doc
 * Name: ase_config_param_functions_table.
 * Type: Array of struct config_param_entry.
 * Description: A global table mapping argus_config_param_name enums and function pointers for parsing values and setting values to default.
 */
extern struct config_param_entry ase_config_param_functions_table[ARGUS_CONFIG_MAX_TYPE];

/** Initializes the configuration parameter functions table */
void init_ase_config_param_functions_table(void);

#endif /** ARGUS_CONFIG_PARAMS_TYPES_H */
