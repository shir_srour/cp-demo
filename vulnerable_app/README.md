## Vulnerable app + lib ##
This folder contains the code for the application which will be executed on the RasPi. 
* demoLib.c, demoLib.h - The legitimate library code
* vulnerableApp.c - The vulnerable application code
* Makefile - makes both library and application according to given targets.
- mal_lib directory:
	* demoLib.c, demoLib.h - The malicious library code
	* Makefile - to make the malicious library

'app' loads a legitimate lib (demoLib.c code - that prints "HELLO WORLD!") in a standard case and when replaced with the malicious lib (mal_lib/demoLib.c), will print a different process registers and save it's heap dump.

## Compiling lib files ##
Use the make file for compiling the lib files, inside the directory use:
'make'

## Compiling the application ##
Inside this directory, use:
'make app'


