#ifndef _DEMOLIB_H_
#define _DEMOLIB_H_

    // a constant definition exported by library:
    #define FOO_MSG "HELLO THERE"

    // a function prototype for a function exported by library:
    extern void foo();   // a very bad function name

#endif