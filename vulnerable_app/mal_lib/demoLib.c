#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/user.h>
#include <sys/syscall.h>
#include <errno.h>
#include <linux/ptrace.h>
#include <sys/mman.h>

void demo()
{
  pid_t target;
  struct user_regs regs;

  // get pid
  char pidline[50];
  FILE *fp = popen("pidof systemd-logind","r");
  fgets(pidline,50,fp);
  target = atoi(pidline);
  pclose(fp);

  // trace this process, print all registers
  printf ("Tracing process %d\n", target);
  if ((ptrace (PTRACE_ATTACH, target, NULL, NULL)) < 0)
    {
      perror ("ptrace ATTACH:");
      exit (1);
    }
  wait (NULL);
  printf ("Process Registers\n");
  ptrace(PTRACE_GETREGS, target, NULL, &regs);
  // print all registers of this process
  printf("ip: %p, fp: %p, lr: %p, cpsr: %p, sp: %p, pc: %p, r0: %ld, r1: %ld, r2: %ld, r3: %ld, r4: %ld, r5: %ld, r6: %ld, r7: %ld, r8: %ld, r9: %ld, r10: %ld \n", (void*)regs.ARM_ip, (void*)regs.ARM_fp, (void*)regs.ARM_lr, (void*)regs.ARM_cpsr, (void*)regs.ARM_sp, (void*)regs.ARM_pc, regs.ARM_r0, regs.ARM_r1, regs.ARM_r2, regs.ARM_r3, regs.ARM_r4, regs.ARM_r5, regs.ARM_r6, regs.ARM_r7, regs.ARM_r8, regs.ARM_r9, regs.ARM_r10);


  char mapsFilename[1024] = "";
  char pid[6];
  sprintf(pid, "%u", target);
  // concate strings to path - /proc/pid/maps
  strcat(mapsFilename, "/proc/");
  strcat(mapsFilename, pid);
  strcat(mapsFilename, "/maps");

  FILE* pMapsFile = fopen(mapsFilename, "r");
  char memFilename[1024] = "";
  // concate strings to path - /proc/pid/mem
  strcat(memFilename, "/proc/");
  strcat(memFilename, pid);
  strcat(memFilename, "/mem");

  FILE* pMemFile = fopen(memFilename, "r");
  char line[256];

  while (fgets(line, 256, pMapsFile) != NULL)
  {
    char* ret;
    // We are only interested in the addresses of the heap
    ret = strstr(line, "[heap]");
    if(ret != NULL){
      printf("line: %s \n", line);
      unsigned long start_address;
      unsigned long end_address;
      // heap start and end addresses
      sscanf(line, "%08lx-%08lx\n", &start_address, &end_address);

      char dd_cmd[1024] = {0};     
      char start_address_str[50];
      sprintf(start_address_str, "%lu", start_address);
      char address_range[50];
      sprintf(address_range, "%lu", (end_address - start_address));

      // generate dd command
      strcat(dd_cmd, "dd if=");
      strcat(dd_cmd, memFilename);
      strcat(dd_cmd, " of=/tmp/mem_dump bs=1 skip=");
      strcat(dd_cmd, start_address_str);
      strcat(dd_cmd, " count=");
      strcat(dd_cmd, address_range);
      printf("dd command: %s",dd_cmd);
      system(dd_cmd);
    }
    
  }
  fclose(pMapsFile);
  fclose(pMemFile);
  ptrace(PTRACE_CONT, target, NULL, NULL);
  ptrace(PTRACE_DETACH, target, NULL, NULL);

}

