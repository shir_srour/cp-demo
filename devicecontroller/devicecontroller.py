import logging

class DeviceController(object):
    fs_signer = None
    private_key_path = None

    def __init__(self, private_key_path=None):
        if private_key_path != None:
            self.private_key_path = private_key_path

    def connect(self):
        pass

    def upload(self, local_path, remote_path):
        pass

    def download(self, remote_path, local_path):
        pass

    def run_command(self, cmd, detached=False):
        """
        Runs the given command.

        Note to mention the full path of the executable, e.g. instead
        of 'rmmod' write '/sbin/rmmod'. See
        https://stackoverflow.com/questions/55419330/some-unix-commands-fail-with-command-not-found-when-executed-using-python-p

        :type cmd: String
        :param cmd: Shell command to execute

        :rtype: The output string of running the command
        """
        logging.debug("CMD - %s", cmd)