from paramiko.ssh_exception import SSHException
from scp import SCPClient
from scp import SCPException

import logging
import paramiko
import subprocess
import os

from .devicecontroller import DeviceController

class SSHController(DeviceController):
    ssh = None
    scp = None

    def __init__(self, private_key_path=None):
        super(SSHController, self).__init__(private_key_path)
        self.ssh = paramiko.SSHClient()

    def connect(self, host, port, username, password):
        if password is None or password == "":
            raise ValueError("No password SSH connection isn't supported. Use a non-empty password")
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
        self.ssh.connect(host, port=port, username=username, password=password)
        self.scp = SCPClient(self.ssh.get_transport())

    def sign_file(self, local_path):
        if os.getuid() != 0:
            raise RuntimeError("You have to run as root in order to sign files")
        # sign file
        print(subprocess.check_output(
            "python3 {script} -p {file} -f".format(script="./scripts/crypto/sign_fs.py",
                                                  file=local_path).split()).decode("utf-8"))
        #print(output)

    def compress_file(self, local_path, remote_path):
        # compress with xattr
        subprocess.check_output("tar -C {dir} --xattrs --xattrs-include='*' -czf ./{file}.tar.gz ./{name}".
                                format(file=local_path, dir=os.path.dirname(local_path), name=os.path.basename(local_path)).split())
        signed_tar_path = "{file}.tar.gz".format(file=local_path)
        remote_tar_path = os.path.dirname(remote_path) + "/" + os.path.basename(signed_tar_path)
        return signed_tar_path, remote_tar_path

    def upload(self, local_path, remote_path):
        # we compress file in case it is signed (so the signature stays in remote)
        local_tar_path, remote_tar_path = self.compress_file(local_path, remote_path)
        try:
            #move signed tar to target
            self.scp.put(local_tar_path, remote_tar_path)
            return True
        except SCPException as e:
            logging.error(str(e))
        except SSHException as e:
            self.scp.put(local_tar_path, remote_tar_path)
        finally:
            #extract tar
            self.run_command("tar --xattrs --xattrs-include='*' -xzmf {0} -C {1}".format(remote_tar_path, os.path.dirname(remote_path)))
            self.run_command("rm {}".format(remote_tar_path))
        return False

    def download(self, remote_path, local_path):
        try:
            self.run_command("tar -C {dir} --xattrs --xattrs-include='*' -czf {file}.tar.gz ./{name}".format(file=remote_path, dir=os.path.dirname(remote_path), name=os.path.basename(remote_path)))
            self.scp.get(remote_path + ".tar.gz", local_path + ".tar.gz")
            self.run_command("rm {}".format(remote_path + ".tar.gz"))
            subprocess.check_output("tar --xattrs --xattrs-include='*' -xzmf {0} -C {1}".format(local_path + ".tar.gz", os.path.dirname(local_path)).split())
        except SSHException as e:
            self.run_command("tar -C {dir} --xattrs --xattrs-include='*' -czf {file}.tar.gz ./{name}".format(file=remote_path, dir=os.path.dirname(remote_path), name=os.path.basename(remote_path)))
            self.scp.get(remote_path + ".tar.gz", local_path + ".tar.gz")
            self.run_command("rm {}".format(remote_path + ".tar.gz"))
            subprocess.check_output("tar --xattrs --xattrs-include='*' -xzmf {0} -C {1}".format(local_path + ".tar.gz", os.path.dirname(local_path)).split())

    def run_command(self, cmd):
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        logging.debug(stderr.read().decode())
        output = stdout.read()
        return output
