import logging
import time

class CPController(object):
    def __init__(self, device_controller):
         self.device_controller = device_controller

    def stop(self):
         self.device_controller.run_command("systemctl stop argus")
         time.sleep(2)
         #self.device_controller.run_command("/sbin/rmmod argus_lsm_module")
         #self.device_controller.run_command("/sbin/rmmod ase")
         logging.info("Stopped CP")

    def start(self):
        #self.device_controller.run_command("reboot")
        #self.device_controller.run_command("systemctl start argus_lsm")
        self.device_controller.run_command("systemctl start argus")
        time.sleep(1)
        logging.info("Started CP")
