
# Prerequisites #

The machine shall have python3, attr and cross compiler installed:

`sudo apt-get install python3 attr gcc-arm-linux-gnueabihf`

Using demo requires some Python modules these can be installed by:

`pip install -r requirements.txt`



## How to use run_demo.py ##

The script has some functionalities which are activated according to the given flags:

* Connect to RPI

	Use the “-c” flag with Raspberry_pi_IP_address:Port:User:Password for example: 
	
	`sudo ./run_demo.py -c 10.64.100.202:22:root:root`
	
	We recommend setting the default value to your ip address (user & password should not change), so you won't need to use this flag with every execution.

	`parser.add_argument('-c', help="Connect to [IP]:[PORT]:[USER]:[PASSWORD]", default="10.64.100.202:22:root:root")`

	In run_demo.py script, inside main function change the bolded IP address to the RasPi IP address. If you don’t change this address you will need to add the “-c” flag with its parameters every time, for example, when using the “-a” flag, the command should look like:

	`sudo ./run_demo.py -c 10.64.100.202:22:root:root -a`


* Mode 0 - changing Argus configuration

	In every scenario the customer may change Argus configuration (Detection / Kill / Prevention), which configures the reaction to anomalous events.
	Use the “-m” flag with one of the values - 'report'/'prevent'/'kill', for example:
	`sudo ./run_demo.py -m report`
	where:
	
	 * report - will write anomalous events to log.
	 
	 * kill - will kill process after executing anomalous event.
	 
	 * prevent - will block anomalous event before happening.
	 

* Mode 1 -  Execution of a legitimate application


	Scenario flow:
	
	Control script runs in mode “1” - execution of vulnerable app.

	Expected Result:
	
	The application performs “HELLO WORLD!”
	Log indicates no alerts


	Execute a legitimate application (a vulnerable one) which prints “HELLO WORLD!”.
	
	Use the “-a” flag, this command uploads app and lib to /etc/argus/vulnerable_app directory and executes app. 
	
	Both app and lib are signed and app is in initial configuration so executing app should not be anomalous:
	
	`sudo ./run_demo.py -a`



* Mode 2 - Drop a malicious lib

	Scenario flow:
	
	Control script runs mode 2 - drops malicious library instead of the legitimate one and executes the vulnerable app.

	Expected Result:
	
	Argus is on ‘Detection’: Vulnerable app peeks another process memory (e.g. in order to extract keys / sensitive data)
	Argus is on ‘Kill’: The process of the vulnerable app is killed after peeking the memory.
	Argus is on ‘Prevention’: The process of the legitimate app fails run the malicious library since it isn’t signed.

	Drops malicious library instead of the legitimate one so vulnerable app uses the malicious lib.
	Use the “-d” flag, dropps the unsigned malicious lib to /etc/argus/vulnerable_app folder and execute app:
	`sudo ./run_demo.py -d`

	This malicious library ptraces a running process named “systemd-logind”, prints it’s registers to screen and download a heap dump to demo/vulnerable_app/heap_dump.


* Mode 3 - Execution of verified script

	Scenario flow:
	
	Signing of the provided shell script (or other customer shell script) and sign it.
	Control script runs mode 3 and uploads the signed script, and then executes it.

	Expected Result: 
		
	Script is executed. Note that if the customer supplies the script, certain commands may be blocked by Argus (e.g. running another process), but the script will execute.
	No alerts in logs related to signature verification (other alerts might occur, depending on what the script does)

	For execution of a verified script, few steps should be done:
	
	1. Sign script
	
	`sudo ./run_demo.py -sign ./script `
	
	2. Add script as a verified script (change configuration so execution will be allowed)
	
	`sudo ./run_demo.py -conf ./script +`
	
	For removing script from allowed executables use the command:
	
	`sudo ./run_demo.py -conf ./script -`
	
	3. Copy script to RasPi and execute
	
	`sudo ./run_demo.py -s ./script`
	
	This command copies script to /etc/argus/files directory in RasPi and executes it.


* Mode 4 - Execution of unknown script

	Scenario flow:
	Control script runs mode 3 and uploads an unsigned script, and then executes it.

	Expected Result:
	
	Argus is on ‘Detection’: Script executes, Signature Verification alert is written to the log.
	
	Argus is on ‘Kill’: The process running the script is killed after it begins to run.
	
	Argus is on ‘Prevention’: The process running the script fails to run.

	In this case we don’t use the “-sign” and “-conf” flags but only the “-s” flag with the path to the script you want to run. 
	
	It copies script to /etc/argus/files directory in RasPi and executes script.
	
	`sudo ./run_demo.py -s ./script`



## The demo directory ##

This library contains all needed items for using the demo. 

1. Control script (For the control machine, written in python), controlling different attack vectors.
“run_demo.py” is the script that demonstrate CP on RPI with the different attack vectors.

2. Vulnerable app (For the target, written in C) installed in root privileges. 
Its code is located in the “vulnerable_app” folder

3. Legitimate lib (For the target written in C) loaded by the app in a standard case.
Its code is located in the “vulnerable_app” folder

4. Malicious lib (For the control machine, written in C).
Its code is located at “vulnerable_app/mal_lib” folder

5. Shell script (For the control machine)
App with overflow (For the control machine)

Some more subdirectories of the demo folder:

* Scripts - a directory that contains two subdirectories:

	- Config - contains scripts that convert configuration files to bin files and vice versa.
	
	- Crypto - this folder contains scripts for signing files

	- sign_yocto_fs.py - script for signing the filesystem of the yocto image while on SD card
	
* Keys - this folder contains keys needed for the signing process

* Devicecontroller - contains auxiliary functions needed for the demo scripts

* Device_config - it contains the configuration files which are downloaded from RasPi while using the control script. Changing the configuration requires more advanced training from Argus.


