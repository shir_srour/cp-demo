#!/usr/bin/env python3
import argparse
import logging
import yaml
import sys
import os
import subprocess
import time
import socket

from devicecontroller.sshcontroller import SSHController
from cp_controller import CPController

local_atd_yaml = "./device_config/atd.yaml"
local_ase_config = "./device_config/ase.config"
local_ase_yaml = "./device_config/ase_config.yaml"
remote_ase_config = "/etc/argus/ase.config"
remote_atd_yaml = "/etc/argus/atd.yaml"
path_to_lib_file = "./vulnerable_app/libdemo.so"
remote_lib_file_path = "/etc/argus/vulnerable_app/libdemo.so"
vulnerable_app_path = "./vulnerable_app/app"
remote_app_path = "/etc/argus/vulnerable_app/app"
path_to_mal_lib_file = "./vulnerable_app/mal_lib/libdemo.so"
remote_files_dir = "/etc/argus/files/"

local_ase_customer = "./device_config/ase_customer.config"
local_ase_customer_yaml = "./device_config/ase_customer.yaml"
remote_ase_customer = "/etc/argus/ase_customer.config"

local_iptables_voilations_rules = "./scripts/network/iptables_voilations_rules.py"
remote_iptables_voilations_rules = "/etc/argus/iptables_voilations_rules.py"

local_argus_iptables = "./device_config/argus_iptables.service"
remote_argus_iptables = "/lib/systemd/system/argus_iptables.service"

default_cfg_dir = "./original_config_files/"
default_ase_cfg = default_cfg_dir + "ase.config"
default_ase_customer_cfg = default_cfg_dir + "ase_customer.config"
default_atd_cfg = default_cfg_dir + "atd.yaml"

class MultilineHandler(logging.StreamHandler):
    def __init__(self):
        super(MultilineHandler, self).__init__()

    def emit(self, record):
        messages = record.msg.split('\n')
        for message in messages:
            if len(message) > 0:
                record.msg = message
                super(MultilineHandler, self).emit(record)

def init_log():
    logging.getLogger("paramiko").setLevel(logging.WARNING)
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    mul_handler = MultilineHandler()
    formatter = logging.Formatter('%(asctime)s - [%(levelname)s] %(message)s')
    mul_handler.setFormatter(formatter)
    log.addHandler(mul_handler)

def run_script(devcon, script_path):
    #upload script
    script_name = os.path.basename(script_path)
    remote_script_path = remote_files_dir + script_name
    if(script_path == script_name):
        script_path = "./" + script_name
    devcon.upload(script_path, remote_script_path)
    print(devcon.run_command(remote_script_path).decode("utf-8"))

def allow_file_exec(cp, devcon, remote_path, add):
    #allow bash to create and execute given file
    cp.stop()
    devcon.download(remote_atd_yaml, local_atd_yaml)
    with open(local_atd_yaml) as atd_file:
        atd_config = yaml.load(atd_file, Loader=yaml.FullLoader)
    if atd_config != None:
    #is file added or removed from config
        if add:
            if(("0~0~" + remote_path) not in (atd_config["ExecuteByParent"]["proc_to_parents"].keys())):
                atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path] = []
            atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path].append("0~0~/bin/bash.bash")
            create_and_exec_whitelist = atd_config["CreateAndExecute"]["process_whitelist"]
            if(remote_path not in create_and_exec_whitelist):
                create_and_exec_whitelist.append(remote_path)
        else:
            while((("0~0~" + remote_path) in (atd_config["ExecuteByParent"]["proc_to_parents"].keys())) and
            (("0~0~/bin/bash.bash") in (atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path]))):
                if(len(atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path]) == 1):
                    del atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path]
                else:
                    atd_config["ExecuteByParent"]["proc_to_parents"]["0~0~" + remote_path].remove("0~0~/bin/bash.bash")
            create_and_exec_whitelist = atd_config["CreateAndExecute"]["process_whitelist"]
            while(remote_path in create_and_exec_whitelist):
                create_and_exec_whitelist.remove(remote_path)
        atd_config["CreateAndExecute"]["process_whitelist"] = create_and_exec_whitelist

    with open(local_atd_yaml, 'w') as atd_file:
        yaml.dump(atd_config, atd_file)
    devcon.sign_file(local_atd_yaml)
    devcon.upload(local_atd_yaml, remote_atd_yaml)
    cp.start()

def get_bin_config(devcon, conf):
    addition = ""
    if conf == "ase_customer":
        remote_conf, local_conf, local_yaml = remote_ase_customer, local_ase_customer, local_ase_customer_yaml
        addition = " --config_header_file ./scripts/argus_customer_config.h"
    else:
        remote_conf, local_conf, local_yaml = remote_ase_config, local_ase_config, local_ase_yaml

    devcon.download(remote_conf, local_conf)
    #bin_to_yaml(local_ase_config, local_ase_yaml)
    subprocess.check_output(
        "python3 {script} -b {bin_file} -o {yaml_file}".format(script="./scripts/config/bin_conf_to_yaml.py" + addition,
                                              bin_file=local_conf, yaml_file=local_yaml).split())

def upload_bin_to_yaml(devcon, conf, default_cfg=False):
    addition = ""
    if conf == "ase_customer":
        if default_cfg:
            remote_conf, local_conf = remote_ase_customer, default_ase_customer_cfg
        else:
            remote_conf, local_conf, local_yaml = remote_ase_customer, local_ase_customer, local_ase_customer_yaml
        addition = " --config_header_file ./scripts/argus_customer_config.h"
    else:
        if default_cfg:
            remote_conf, local_conf = remote_ase_config, default_ase_cfg
        else:
            remote_conf, local_conf, local_yaml = remote_ase_config, local_ase_config, local_ase_yaml

    if not default_cfg:
        subprocess.check_output(
            "python3 {script} -y {yaml_file} -o {bin_file}".format(
                script="./scripts/config/yaml_conf_to_bin.py" + addition,
                yaml_file=local_yaml, bin_file=local_conf).split())
    devcon.sign_file(local_conf)
    devcon.upload(local_conf, remote_conf)

def set_ase_config_values(bool_val, devcon):
    get_bin_config(devcon, "ase")
    with open(local_ase_yaml) as ase_config_file:
        ase_config = yaml.load(ase_config_file, Loader=yaml.FullLoader)

        ase_config["ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITHOUT_SIGNATURE"] = bool_val
        ase_config["ARGUS_CONFIG_SHOULD_PREVENT_FILES_WITH_INVALID_SIGNATURE"] = bool_val
        ase_config["ARGUS_CONFIG_SHOULD_PREVENT_PTRACE"] = bool_val

    with open(local_ase_yaml, 'w') as ase_config_file:
        yaml.dump(ase_config, ase_config_file)
    upload_bin_to_yaml(devcon, "ase")


def set_atd_config_action(action, devcon):
    devcon.download(remote_atd_yaml, local_atd_yaml)
    with open(local_atd_yaml) as atd_file:
        atd_config = yaml.load(atd_file, Loader=yaml.FullLoader)

        atd_config["ExecuteByParent"]["action"] = action
        atd_config["CreateAndExecute"]["action"] = action

    with open(local_atd_yaml, 'w') as atd_file:
        yaml.dump(atd_config, atd_file)
    devcon.sign_file(local_atd_yaml)
    devcon.upload(local_atd_yaml, remote_atd_yaml)

def change_cp_mode(mode, devcon):
    if mode == "prevent":
        set_ase_config_values(True, devcon)
        set_atd_config_action("log_action", devcon)
    else:
        set_ase_config_values(False, devcon)
        if mode == "kill":
            set_atd_config_action("process_kill_action", devcon)
        elif mode == "report":
            set_atd_config_action("log_action", devcon)
    print("################################### Reboot RasPi to load ASE configurations! ###################################")


def run_app(devcon):
    # compile + sign application and lib
    subprocess.Popen("make", stdout=subprocess.PIPE, cwd="./vulnerable_app", shell=True)
    devcon.sign_file("./vulnerable_app/libdemo.so")
    time.sleep(2)
    subprocess.Popen("make app", stdout=subprocess.PIPE, cwd="./vulnerable_app", shell=True)
    devcon.sign_file("./vulnerable_app/app")

    devcon.run_command("mkdir -p /etc/argus/vulnerable_app")
    # upload app + lib file
    devcon.upload(vulnerable_app_path, remote_app_path)
    devcon.upload(path_to_lib_file, remote_lib_file_path)
    #run it
    print(devcon.run_command(remote_app_path).decode("utf-8"))

def replace_lib(devcon):
    # compile malicious lib
    subprocess.Popen("make", stdout=subprocess.PIPE, cwd="./vulnerable_app/mal_lib", shell=True)
    time.sleep(2)
    #make sure app is on machine
    subprocess.Popen("make app", stdout=subprocess.PIPE, cwd="./vulnerable_app", shell=True)
    devcon.sign_file("./vulnerable_app/app")
    #upload an *unsigned* malicious lib
    devcon.run_command("rm " + remote_lib_file_path)
    devcon.upload(path_to_mal_lib_file, remote_lib_file_path)
    #run it
    print(devcon.run_command(remote_app_path).decode("utf-8"))

def update_synflood_cfg(cp, devcon, interface):
    cp.stop()
    devcon.download(remote_atd_yaml, local_atd_yaml)
    with open(local_atd_yaml) as atd_file:
        atd_config = yaml.load(atd_file)
    if atd_config != None:
        atd_config["SynFlood"]["MonitoredNetworkInterfaces"][interface] = {"limit": 10, "time_frame_in_seconds": 10}

    with open(local_atd_yaml, 'w') as atd_file:
        yaml.dump(atd_config, atd_file)
    devcon.sign_file(local_atd_yaml)
    devcon.upload(local_atd_yaml, remote_atd_yaml)
    cp.start()


def update_high_packet_rate_cfg(cp, devcon, interface):
    cp.stop()
    devcon.download(remote_atd_yaml, local_atd_yaml)
    with open(local_atd_yaml) as atd_file:
        atd_config = yaml.load(atd_file, Loader=yaml.FullLoader)
    if atd_config != None:
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["destination_port"] = "all"
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["direction"] = "INPUT"
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["interface_name"] = interface
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["limit"] = 10
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["protocol"] = "all"
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["source_port"] = "all"
        atd_config["HighPacketRate"]["MonitoredPacketRates"][0]["time_frame_in_seconds"] = 5

    with open(local_atd_yaml, 'w') as atd_file:
        yaml.dump(atd_config, atd_file)
    devcon.sign_file(local_atd_yaml)
    devcon.upload(local_atd_yaml, remote_atd_yaml)
    cp.start()

def main():
    parser = argparse.ArgumentParser(description="CP demo options")
    parser.add_argument('-app', help="Run an application", action='store_true')
    parser.add_argument('-c', help="Connect to [IP]:[PORT]:[USER]:[PASSWORD]",
                        default="10.64.100.74:22:root:root")
    parser.add_argument('-mal_lib', help="Drop a malicious Library", action='store_true')
    parser.add_argument('-change_mode', help="Change CP mode", choices=['report', 'prevent', 'kill'])
    parser.add_argument('-conf', type=str, help="Add or remove executable to configuration so it can be executed. Executable must be placed in " + remote_files_dir + ". Does NOT upload the executable, just adds it to atd.yaml ExecuteByParent. mode can be either '+' (add to conf) or '-' (remove from conf)", nargs=2, metavar=('executable', 'mode'))
    parser.add_argument('-upload', type = str, help="Sign and upload a file to target. local_path must be a relative path (e.g. ./script, but NOT script)", nargs=2, metavar=('local_path', 'remote_path'))
    parser.add_argument('-download_cfg', help="Download configuration file and convert to yaml if needed", choices=['atd', 'ase', 'ase_customer'])
    parser.add_argument('-reset_cfg', help="Upload all default configurations to target", action='store_true')
    parser.add_argument('-run', type=str, help="Upload the script passed as parameter in " + remote_files_dir + "on the Rpi, and tries and run it. Does NOT sign the script.")
    parser.add_argument('-sign', type=str, help="Sign file")
    parser.add_argument('-upload_cfg', help="Upload configuration file - sign and convert to bin if needed", choices=['atd', 'ase', 'ase_customer'])
    parser.add_argument('-synflood', type=str, help="Demonstrates Synflood feature. Describe interface name to send syn packets")
    parser.add_argument('-highpacket', type=str, help="Demonstrates High Packet Rate feature. Describe interface name to send packets")
    parser.add_argument('-halfopen', help="Demonstrates Half Open Connections feature", action='store_true')

    args = parser.parse_args()

    init_log()

    ip, port, user, password = args.c.split(':')
    devcon = SSHController("keys/hips.key")
    devcon.connect(ip, port, user, password)

    if devcon is not False:

        cp = CPController(devcon)

        if args.change_mode:
            cp.stop()
            # Update + Upload conf
            change_cp_mode(args.change_mode, devcon)
            cp.start()

        if args.run:
            #upload and run scripts in RasPi
            input_path = args.run
            if not os.path.isfile(input_path):
                print('The file specified does not exist')
                sys.exit()
            devcon.run_command("mkdir -p " + remote_files_dir)
            run_script(devcon, args.run)

        if args.sign:
            #sign file
            input_path = args.sign
            if not os.path.isfile(input_path):
                print('The file specified does not exist')
                sys.exit()
            devcon.sign_file(input_path)

        if args.conf:
            #add/remove file from configuration so execution is allowed/disallowed
            filename = os.path.basename(args.conf[0])
            devcon.run_command("mkdir -p " + remote_files_dir)
            add = False
            if(args.conf[1] == "+"):
                add = True
            allow_file_exec(cp, devcon, remote_files_dir + filename, add)

        if args.app:
            #run vulnerable app
            run_app(devcon)

        if args.mal_lib:
            replace_lib(devcon)
            # download heap dump to local machine if generated
            if ("mem_dump" in devcon.run_command("ls /tmp/").decode("utf-8")):
                devcon.download("/tmp/mem_dump", "./vulnerable_app/heap_dump")

        if args.download_cfg:
            if (args.download_cfg == "atd") :
                devcon.download(remote_atd_yaml, local_atd_yaml)
            else :
                get_bin_config(devcon, args.download_cfg)

        if args.upload_cfg:
            cp.stop()
            if (args.upload_cfg == "atd") :
                devcon.sign_file(local_atd_yaml)
                devcon.upload(local_atd_yaml, remote_atd_yaml)
            else :
                upload_bin_to_yaml(devcon, args.upload_cfg)
                print("################################### Reboot RasPi to load ASE configurations! ###################################")
            cp.start()

        if args.reset_cfg:
            cp.stop()
            devcon.sign_file(default_atd_cfg)
            devcon.upload(default_atd_cfg, remote_atd_yaml)
            upload_bin_to_yaml(devcon, "ase", True)
            upload_bin_to_yaml(devcon, "ase_customer", True)
            print("################################### Reboot RasPi to load ASE configurations! ###################################")
            cp.start()

        if args.upload:
            (local_path, remote_path) = args.upload
            devcon.sign_file(local_path)
            devcon.upload(local_path, remote_path)

        if args.synflood or args.highpacket:
            if args.synflood:
                interface = args.synflood
                update_synflood_cfg(cp, devcon, interface)
            elif args.highpacket:
                interface = args.highpacket
                update_high_packet_rate_cfg(cp, devcon, interface)
            ##create new argus-iptables.service to load new config
            subprocess.check_output("./scripts/network/iptables_rules.sh ./device_config/atd.yaml ./device_config/argus_iptables.service ./scripts/network/service_template ./scripts/network/create_iptables_rules.py", shell=True)
            ##upload new argus-iptables.service to to update iptables
            devcon.upload(local_argus_iptables, remote_argus_iptables)
            devcon.run_command("/usr/sbin/iptables -F")
            devcon.run_command("/usr/sbin/iptables -X")
            devcon.run_command("systemctl daemon-reload")
            devcon.run_command("systemctl restart argus_iptables")
            devcon.run_command("timeout 100 nc.netcat-openbsd -l 54321 &")
            subprocess.check_output("sudo hping3 -c 400 -S -p 54321 --flood -V " + ip, shell=True)

        if args.halfopen:
            devcon.sign_file("./scripts/network/server-script.py")
            devcon.upload("./scripts/network/server-script.py", "/etc/argus/server-script.py")
            devcon.run_command("python /etc/argus/server-script.py &")
            subprocess.check_output("sudo hping3 -c 400 -S -p 45200 --flood -V " + ip, shell=True)

    logging.info("Done!")

if __name__ == "__main__":
    main()

